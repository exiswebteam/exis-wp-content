<?php

/**
 *
 * Template Name: Software Inner
 */


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="site-main" id="main">

                <?php get_template_part( 'page-templates/content', 'page-header' ); ?>

                <section class="container page-content-section">
                    <div class="vc_section">
                        <?php
                        the_content();
                        ?>
                    </div>
                </section>
                <section id="system-integration" class="d-flex system-integration forSoftware">
                    <div class="system-integration__left forSoftware__left">
                        <h2>
                            <?= __('Business <br/>flexibility <br/>requires custom software', 'exis'); ?>
                        </h2>
                        <p>
                            <?= __('We work with', 'exis'); ?>
                        </p>
                        <ul>
                        	<li><?= __('Multinationals', 'exis');?></li>
                        	<li><?= __('Enterprises', 'exis');?></li>
                        	<li><?= __('SMEs', 'exis');?></li>
                        	<li><?= __('Startups', 'exis');?></li>
                        </ul>
                      <?php if(!wp_is_mobile()):?>
                     <div class="system-intergration__arrow--left">
                       <a class="arrow-left-square svg-arrow" href="#"></a>
                    </div>
                    <?php endif;?>
                    </div>
                    <div class="system-integration__cont">
                        <div class="d-flex system-integration__right">
                              <?php
                            if( have_rows('services') ):
                                while ( have_rows('services') ) : the_row(); ?>

                                    <div class="system-integration__item">
                                        <h3>
                                            <?php
                                            the_sub_field('title');
                                            ?>
                                        </h3>
                                        <div class="system-integration__item-desc">
                                            <?php
                                            the_sub_field('description');
                                            ?>
                                        </div>
                                    </div>
                                <?php
                                endwhile;

                            endif;

                            ?>
                        </div>
                       <?php if(wp_is_mobile()):?>
                      	 <p class="scroll-text">
                           <a class="arrow-left-black arrow-black" href="#"></a>
                       		 swipe
                           <a class="arrow-right-black arrow-black" href="#"></a>
                        </p>
                      <?php endif; ?>
                    </div>
                    <?php if(!wp_is_mobile()):?>
                     <div class="system-intergration__arrow--right">
                       <a class="arrow-right-square svg-arrow" href="#"></a>
                     </div>
                    <?php endif; ?>
                </section>
                <section id="areas-expertise" class="areas-expertise">
                    <div id="areas-expertise__inner"  class="d-flex">
                           <div class="areas-expertise__left">
                                <h2 class="mb-0">
                                    <?= __('<strong>Unique</strong> businesses, <br>unique <strong>challenges</strong>', 'exis'); ?>
                                </h2>
                                <p>
                                <?= __('Businesses of any size across industries, turn to us for the design and development of software solutions built for the unique challenges of their needs.'); ?>
                                </p>
                                <a class="our-work-link" href="<?= get_permalink(123); ?>"><?= __('See our work', 'exis');?></a>
                            </div>
                          <div class="areas-expertise__right">
                              <div class="areas-expertise__right--inner">
                              	<p><?= __('Industries we have served', 'exis'); ?></p>
                              	<div class="row areas-icons">
                                  <?php
                                  if( have_rows('industry_logos_repeater') ):
                                    while ( have_rows('industry_logos_repeater') ) : the_row();
                                    $logo = get_sub_field('logo');
                                    $title = get_sub_field('title');
                                    ?>
                                    <div class="col-6 col-sm-3 areas-icons__item">
                                			 <img src="<?= $logo['url']; ?>" alt="<?= $logo['alt'].' '.$title; ?>">
                                			 <h5><?= $title; ?></h5>
                                		</div>
                                    <?php
                                    endwhile;
                                  endif;
                                  ?>
                              	</div>
                              </div>
                          </div>
                 	 </div>
                </section>
                <section id="our-process" class="d-flex  align-items-center our-process">
                    <div class="our-process__left">
                        <h2 class="mb-0">
                            <?= __('Our<br/>approach', 'exis'); ?>
                        </h2>
                    </div>
                    <div class="our-process__right">

                        <div class="step-process step-1">
                            <div class="step-process__number">
                                <span>1</span>
                            </div>
                            <div class="step-process__text">
                                <h3><?= __('Requirements Analysis', 'exis'); ?></h3>
                                <p><?= __('We work closely with you and your key personnel in order to translate your business needs into specific software features.', 'exis'); ?></p>
                            </div>
                        </div>
                        <div class="step-process step-2">
                            <div class="step-process__number">
                                <span>2</span>
                            </div>
                            <div class="step-process__text">
                                <h3><?= __('Design &amp; Development', 'exis'); ?></h3>
                                <p><?= __('Our agile software development process ensures hassle-free and quick deployment.', 'exis'); ?></p>
                            </div>
                        </div>
                        <div class="step-process step-3">
                            <div class="step-process__number">
                                <span>3</span>
                            </div>
                            <div class="step-process__text">
                                <h3><?= __('Delivery &amp; Support', 'exis'); ?></h3>
                                <p><?= __('We are always there for you to support your systems, thus you are free to focus on your job, growing your business.', 'exis'); ?></p>
                            </div>
                        </div>
                    </div>
                </section>

				<?php get_template_part( 'page-templates/frontpage/content', 'clients' ); ?>

				<section class="container px-0 websites-logo-section">
                    <h5 class="text-center"><?= __('Trusted by', 'exis'); ?></h5>
                    <div class="websites-logo">
                        <?php
                        if( have_rows('logos_software') ):
                            while ( have_rows('logos_software') ) : the_row(); ?>

                                <div class="websites-logo__item">
                                    <img src="<?php the_sub_field('image'); ?>" alt="<?php the_title(); ?>">
                                </div>
                            <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </section>
				 <section class="websites-technologies">
                    <div class="container">
                        <h5><?= __('Technologies', 'exis'); ?></h5>
                        <div class="technologies-container">
                            <?php if( get_field('technologies') ): ?>
                               <?php the_field('technologies'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
                <section class="container px-0 websites-projects">
                    <h2><?= __('Our Track Record', 'exis'); ?></h2>
                    <div class="websites-projects__numbers">
                        <div class="text-center websites-projects__item">
                            <span><?php echo date("Y") - 2001 ?></span>
                            <p><?= __('years of experience', 'exis'); ?></p>
                        </div>
                        <div class="text-center websites-projects__item">
                            <span><?=  get_field_object('custom_software', 'option')['value']; ?></span>
                            <p><?= get_field_object('custom_software', 'option')['label']; ?></p>
                        </div>
                        <div class="text-center websites-projects__item">
                            <span><?=  get_field_object('crm_related', 'option')['value']; ?></span>
                            <p><?= get_field_object('crm_related', 'option')['label'];  ?></p>
                        </div>
                        <div class="text-center websites-projects__item">
                            <span><?=  get_field_object('custom_business_softwares', 'option')['value']; ?></span>
                            <p><?= get_field_object('custom_business_softwares', 'option')['label']; ?></p>

                        </div>
                    </div>
                    <div class="text-right">
                        <a class="our-work-link no-arrow" href="<?= get_permalink(123); ?>"><?= __('See our work', 'exis');?></a>
                    </div>
                </section>
                <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
