<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>
<?php
$web_info = get_field('web_info');
?>
<div class="wrapper" id="single-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">
            <main class="single-site-main" id="single-main">
                <?php get_template_part( 'page-templates/content', 'header-projects' ); ?>
                <section class="header-single-row">
                    <div class="container">
                        <h1 class="single-title-heading"><?php the_title();?></h1>
                        <div class="row align-items-start single-client-info">
                            <div class="col-md-6 d-flex project-info-box">
                                <div class="project-info__label">
                                    <div><?= __('Client', 'exis'); ?></div>
                                </div>
                                <div class="project-info__desc">
                                    <div class="project-info__client-desc">
                                    <?= $web_info['client']; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 text-center logo-client-section">
                                <img class="logo-client" src="<?= $web_info['logo']; ?>" alt="logo-client">
                            </div>
                        </div>
                    </div>
                </section>

                <section class="fixed-slider-section">
                    <div id="sticky-menu" class="left-sidebar">
                        <div class="slider-content-description">
                            <div class="slider-content-description__div">
                                <?= get_field('slider_description') ?>
                                <?php
                                if(get_field('url')):
                                ?>
                                <p class="text-left">
                                    <a class="gotoproject-btn" href="<?= the_field('url'); ?>" target="_blank">
                                        <?= __('visit website','exis');?>
                                    </a>
                                </p>
                                <?php
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="right-sidebar">
                        <?php

                        if( have_rows('slider_images') ):
                            while ( have_rows('slider_images') ) : the_row();?>
                                <div>
                                    <img src="<?php the_sub_field('image'); ?>" alt="clients-screen">
                                </div>
                            <?php
                            endwhile;
                        endif;

                        ?>

                    </div>
                </section>


                <section class="outcome-single-row">
                    <div class='container-fluid outcome-section'>
                        <div class='container'>
                            <div class='project-info__label'>
                                <div>
                                    <h2>
                                        <?= __('Outcome <br/> & Benefit', 'exis'); ?>
                                    </h2>
                                </div>
                            </div>
                            <div class='row software-container__inner'>
                                <div class='col-md-11 d-flex project-info-box'>
                                    <div class='project-info__desc'>
                                        <div>
                                            <?php echo $web_info['outcome_benefit']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class='container technologies-section'>
                        <div class='text-center technologies-section__inner'>
                            <h5><?= __('Technologies used'); ?></h5>
                            <div class='technologies-section__list'>
                                <ul>
                                    <?php
                                    $technologies = get_field('technologies');
                                    ?>
                                    <?php
                                    foreach($technologies as $technology) {
                                        ?>
                                        <li><?php echo $technology['tech_name']; ?></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>

                <?php
                $category = get_the_terms( $post->ID, 'web_categories' );
                $termID = $category[0]->term_id;
                $args = array(
                    'post_type' => array('web_projects'),
                    'post_status' => array('publish'),
                    'post__not_in' => array($post->ID),
                    'posts_per_page' => 3,
                    'orderby' => 'rand',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'web_categories',
                            'field' => 'term_id',
                            'terms' => $termID
                        )
                    )
                );

                $query = new WP_Query( $args );
                // The Query
                if ( $query->have_posts() ) {

                ?>
                <section class="related-projects-row">
                    <h2 class="text-center related-projects__title">
                        <?= __('Other Web Projects', 'exis'); ?>
                    </h2>
                    <div class="container related-container">
                        <div class="row">
                        <?php

                         while ($query->have_posts()) {
                                $query->the_post(); ?>

                        <article class="col-md-4 web-item">
                            <a href="<?= get_permalink(); ?>">
                                <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                            </a>

                            <header class="web-entry-header">
                                <div class="heading-container">
                                    <?php
                                    the_title(
                                        sprintf( '<h2 class="web-entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
                                        '</a></h2>'
                                    );
                                    ?>
                                    <?php if( get_field('web_projects_category') ): ?>
                                        <p><?php the_field('web_projects_category'); ?></p>
                                    <?php endif; ?>
                                </div>
                                <div class="excerpt-container">
                                    <?php the_excerpt(); ?>
                                </div>
                            </header>
                            <div class="text-right view-more-section view-more--web">
                                <a href="<?= get_permalink(); ?>"><?= __('View more', 'exis'); ?></a>
                            </div>
                        </article>
                            <?php } ?>
                        </div>
                    </div>
                </section>
                    <?php
                }
                ?>

                <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #single-wrapper -->
<?php get_footer(); ?>
