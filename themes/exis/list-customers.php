<?php
/**
 *
 * Template Name: List Customers
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>
<?php
//$array = [
//    "Κέντρο Τεχνολογικής Ανάπτυξης Αττικής",
//    "ΝΙ.ΜΑ.",
//    "Οργανισμός Ασφάλισης Προσωπικού ΔΕΗ",
//    "Σύλλογος Γονέων και Κηδεμόνων Σχολής Μοραΐτη",
//    "Ταμείο Αρχαιολογικών Πόρων",
//
//];
//foreach ($array as $item){
//$row = array(
//    'customer'	=> $item,
//);
//
// add_row('field_5d3586fa6e773', $row);
//}

?>
<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="site-main" id="main">

                <?php get_template_part( 'page-templates/content', 'header-projects' ); ?>

                <section class="projects-container list-container">
                    <div class="container">
                        <div class="list-container__inner">
                            <h3 class="list-customers-title"><?= __('See all of our customers in a comprehensive alphabetical list.', 'exis'); ?></h3>

                            <div class="list-customers-section">
                                <?php
                                $fields = get_field_objects();
                                if( $fields )
                                {
                                    foreach( $fields as $field_name => $field )
                                    { ?>
                                        <?php
                                        if(have_rows($field['key'])):
                                            ?>
                                        <div id="row-<?= $field['label'] == '#' ? 'spec' : $field['label']; ?>" class="row-letter-customers">
                                            <h3><?= $field['label']; ?></h3>
                                            <div class="row-letter-customers__list">
                                                <ul class="row">

                                                    <?php // Get repeater value
                                                    $repeater = get_field($field['name']);
                                                    $the_name = array();
                                                    // Obtain list of columns
                                                    foreach ($repeater as $key => $row) {
                                                        $the_name[$key] = $row['customer'];
                                                    }

                                                    // Sort the data by name column, ascending
                                                    array_multisort($the_name, SORT_ASC, $repeater);?>

                                                    <?php if($repeater): ?>
                                                        <?php foreach( $repeater as $row ) {  ?>
                                                            <li class="col-sm-4"><?php echo $row['customer'];?></li>
                                                        <?php } ?>
                                                    <?php endif;?>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php endif; ?>

                                    <?php }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="left-fixed-menu list-customers-menu">
                    <h5><?= __('OUR CUSTOMERS', 'exis'); ?></h5>
                    <ul class="row">
                        <?php

                        foreach (range('A', 'Z') as $char) {
                            echo '<li class="col-4" data-letter="'.$char.'">'.$char.'</li>';
                        }
                            echo '<li class="col-4" data-letter="spec">#</li>';
                        ?>
                    </ul>
                </div>
                <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
