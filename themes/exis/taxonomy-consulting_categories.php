<?php


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}


get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="site-main" id="main">

                <?php get_template_part( 'page-templates/content', 'header-projects' ); ?>
                <section class="submenu-consulting-section">
                    <ul class="navbar-nav submenu-consulting-list">
                        <?php
                        $args = array(
                            'taxonomy' => 'consulting_categories',
                            'hide_empty' => false
                        );

                        $cats = get_categories($args);

                        foreach($cats as $cat) {
                            ?>
                            <li class="nav-item <?php echo is_tax('consulting_categories', $cat->term_id) ? 'active' : '' ?>  projects-categories__list-item consult-menu-item">
                                <a class="nav-link" href="<?= get_category_link($cat->term_id); ?>">
                                    <?php
                                    echo $cat->name;
                                    ?>
                                </a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>

                </section>
                <section class="projects-container consult-container">
                    <div class="container">
                        <h2 class="headline-section__title headline-section__title--consult "><?= __('Indicative Work', 'exis'); ?></h2>
                        <div class="consult-container__inner clearfix">

                            <?php
                            $category = get_queried_object();
                               $args = array(
                                   'post_type' => 'consulting_projects',
                                   'orderby'=>'menu_order',
                                   'tax_query' => array(
                                       array(
                                           'taxonomy' => 'consulting_categories',
                                           'field'    => 'term_id',
                                           'terms'    => $category->term_taxonomy_id,
                                       ),
                                   ),
                               );
                               $query = new WP_Query( $args );
                               if ( $query->have_posts() ) : ?>

                                <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                                    <article class="d-flex consult-item">
                                        <div class="logo-consult">
                                            <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                                        </div>
                                        <div class="consult-excerpt-container">
                                            <?php the_content(); ?>
                                        </div>
                                    </article>

                                <?php endwhile; ?>

                            <?php else : ?>

                                <?php get_template_part( 'loop-templates/content', 'none' ); ?>

                            <?php endif; ?>
                        </div>
                        <div class="web-pagination">
                            <?php understrap_pagination(); ?>
                        </div>
                    </div>
                     <section class="container px-0 websites-projects">
                        <h2><?= __('Our Track Record', 'exis'); ?></h2>
                        <div class="websites-projects__numbers">
                            <div class="text-center websites-projects__item">
                                <span><?php echo date("Y") - 2001 ?></span>
                                <p><?= __('years of experience', 'exis'); ?></p>
                            </div>
                            <div class="text-center websites-projects__item">
                                <span><?=  get_field_object('consulting', 'option')['value']; ?></span>
                                <p><?=  get_field_object('consulting', 'option')['label'];?></p>
                            </div>
                            <div class="text-center websites-projects__item">
                                <span><?=  get_field_object('software_consulting', 'option')['value']; ?></span>
                                <p><?= get_field_object('software_consulting', 'option')['label'];  ?></p>
                            </div>
                            <div class="text-center websites-projects__item">
                                <span><?=  get_field_object('business_consulting', 'option')['value']; ?></span>
                                <p><?= get_field_object('business_consulting', 'option')['label']; ?></p>
                            </div>
                        </div>
                        <div class="text-right">
                            <a class="our-work-link no-arrow" href="<?= get_permalink(159); ?>"><?= __('See list of all customers', 'exis'); ?></a>
                        </div>
                    </section>
                </section>

                <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
