<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}


get_header();

$container = get_theme_mod( 'understrap_container_type' );

//global $wp_query;

//print_r($wp_query);
?>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="site-main" id="main">

                <?php get_template_part( 'page-templates/content', 'header-projects' ); ?>

                <section class="projects-container web-container">
                    <div class="container">
                       <h2 class="headline-section__title headline-section__title--web"><?= __('Indicative Work', 'exis'); ?></h2>
                        <div class="row web-container__inner clearfix">

                    <?php if ( have_posts() ) : ?>

                        <?php while ( have_posts() ) : the_post(); ?>

                            <article class="col-md-4 web-item">
                                <a href="<?= get_permalink(); ?>">
                                    <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                                </a>

                                <header class="web-entry-header">
                                    <div class="heading-container">
                                        <?php
                                        the_title(
                                            sprintf( '<h2 class="web-entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
                                            '</a></h2>'
                                        );
                                        ?>
                                        <?php if( get_field('web_projects_category') ): ?>
                                            <p><?php the_field('web_projects_category'); ?></p>
                                        <?php endif; ?>
                                    </div>
                                    <div class="excerpt-container">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </header>
                                <div class="text-right view-more-section view-more--web">
                                    <a href="<?= get_permalink(); ?>"><?= __('View more', 'exis'); ?></a>
                                </div>
                            </article>

                        <?php endwhile; ?>

                    <?php else : ?>

                        <?php get_template_part( 'loop-templates/content', 'none' ); ?>

                    <?php endif; ?>
                        </div>
                        <div class="web-pagination">
                            <?php understrap_pagination(); ?>
                        </div>
                    </div>
                     <section class="container px-0 websites-projects">
                        <h2><?= __('Our Track Record', 'exis'); ?></h2>
                        <div class="websites-projects__numbers">
                            <div class="text-center websites-projects__item">
                                <span><?php echo date("Y") - 2001 ?></span>
                                <p><?= __('years of experience', 'exis'); ?></p>
                            </div>
                            <div class="text-center websites-projects__item">
                                <span><?=  get_field_object('websites', 'option')['value']; ?></span>
                                <p><?=  get_field_object('websites', 'option')['label'];?></p>
                            </div>
                            <div class="text-center websites-projects__item">
                                <span><?=  get_field_object('e-shops', 'option')['value']; ?></span>
                                <p><?= get_field_object('e-shops', 'option')['label'];  ?></p>
                            </div>
                            <div class="text-center websites-projects__item">
                                <span><?=  get_field_object('web_integrations', 'option')['value']; ?></span>
                                <p><?= get_field_object('web_integrations', 'option')['label']; ?></p>
                            </div>
                        </div>
                        <div class="text-right">
                            <a class="our-work-link no-arrow" href="<?= get_permalink(159); ?>"><?= __('See list of all customers', 'exis'); ?></a>
                        </div>
                    </section>
                </section>


                <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
