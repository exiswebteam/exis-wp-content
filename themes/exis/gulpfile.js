//npm packages
var argv = require('minimist')(process.argv.slice(2));
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var jshint = require('gulp-jshint');
var gulpif = require('gulp-if');
var lazypipe = require('lazypipe');
var cleanCSS  = require('gulp-clean-css');
var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var flatten = require('gulp-flatten');
var svgmin = require('gulp-svgmin');
var sourcemaps = require('gulp-sourcemaps');
var runSequence  = require('run-sequence');


// CLI options
var enabled = {
  // Fail due to JSHint warnings only when `--production`
  failJSHint: argv.production,
  //SourceMaps
  maps: !argv.production,
  // Strip debug statments from javascript when `--production`
  stripJSDebug: argv.production
};


//Localhost
var LOCALHOST = 'http://localhost:8888/exis';

//EXIS JS libraries - bad practice
var JAVASCRIPT_LIBRARIES = [
  'assets/js/ScrollMagic.min.js',//maybe remove
  'assets/js/anime.min.js',
  'assets/js/slick.min.js',
  'assets/js/aos.js',
  'assets/js/history.js',
  'assets/js/main.js'
];


// Set the browsers support for css
const AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];


// ###Styles task
gulp.task('styles', function() {
    gulp.src('assets/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: AUTOPREFIXER_BROWSERS,
            cascade: false
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({
          outputStyle: 'compressed',
        }))
        .pipe(sourcemaps.write('.', {
          sourceRoot: 'assets/sass/'
        }))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
});


// ###Image minify
gulp.task('imagemin', function() {
   var imgSrc = 'assets/images/*.+(png|jpg|gif|svg)',
   imgDst = './dist/images';

   return gulp.src(imgSrc)
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: true}]
    }))
   //gulp.src(imgSrc)
   .pipe(changed(imgDst))
   .pipe(imagemin())
   .pipe(gulp.dest(imgDst))
   .pipe(gulp.dest(imgDst))
   .pipe(browserSync.stream());
});


// ### Clean
gulp.task('clean', require('del').bind(null, ['./dist']));


// ###javascript libraries
gulp.task('scripts', function() {
    return gulp.src(JAVASCRIPT_LIBRARIES)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream());
});


// ### JSHint
gulp.task('jshint', function() {
  return gulp.src('assets/js/main.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(gulpif(enabled.failJSHint, jshint.reporter('fail')));
});


// ### Fonts
gulp.task('fonts', function() {
  return gulp.src('assets/fonts/*')
    .pipe(flatten())
    .pipe(gulp.dest('./dist/fonts'))
    .pipe(browserSync.stream());
});


// ###### Watch task: gulp watch
gulp.task('watch',function() {
  browserSync.init({
    files: ['{lib,templates}/**/*.php', '*.php'],
    proxy: LOCALHOST,
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  });
  gulp.watch('assets/sass/**/*.scss',['styles']);
  gulp.watch('assets/js/main.js',['scripts']);
  gulp.watch('assets/js/main.js', ['jshint', 'scripts']);
  gulp.watch('assets/images/*.+(png|jpg|gif)',['imagemin']);
  gulp.watch('assets/fonts/*',['fonts']);
});


// ###### Build task: gulp --production
gulp.task('build', function(callback) {
  runSequence('styles','scripts',['imagemin', 'fonts'],callback);
});


// ###### task: gulp
gulp.task('default', ['clean'], function() {
  gulp.start('build');
});
