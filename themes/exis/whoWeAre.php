<?php
/**
 *
 * Template Name: Who we are
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="who-page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="site-main" id="main">


                <?php get_template_part( 'page-templates/content', 'page-header-who' ); ?>

				<h2 class="text-center who-we-are__title"><?= __('EXIS Management Team', 'exis'); ?></h2>

                <section class="row who-members">

                    <?php

                    if( have_rows('members') ):
                        while ( have_rows('members') ) : the_row();?>
                            <div class="text-center col-sm-6 who-members__item animated fadeInUp">
                                <img src="<?php the_sub_field('image'); ?>" alt="member">
                                <div class="text-left who-members__desc">
                                    <?php the_sub_field('description'); ?>
                                </div>
                            </div>
                        <?php
                        endwhile;
                    endif;

                    ?>

                </section>

                <section class="who-description-section">
                  <p class="text-center">
                      We are a team of tech experts with <?= date("Y")-2001; ?> years long experience in designing and building technology powered solutions, that improve business performance through digital transformation and business automation.
                  </p>
                </section>

                <div class="mindset-section">
                    <?php $bg_top = get_field('background_image_top'); ?>
                		<img src="<?= $bg_top['url']; ?>" alt="<?= $bg_top['alt'].''.get_the_title(); ?>">
                    <div class="hover-elements">
                          <div class="d-flex h-100 align-items-center container justify-content-center">
                            <div data-aos="fade-right" class="page-title white-font">
                                <h1>Our Purpose</h1>
                            </div>
                            <div data-aos="fade-left"  data-aos-delay="100" class="page-content white-font who-content">
                                <p>We share with our clients, regardless of their size, the fascinating experience of discovering how technology can improve their efficiency and their business performance.</p>
                            </div>
                         </div>
                    </div>
                </div>

                <section class="who-more-section">
                    <div class="container who-more-section__inner">
                        <p>Once we <strong>partner with a client</strong> we dedicate ourselves to offering the best value for money solutions. Our clients choose to <strong>stay with us</strong> over the years and across their technology related needs.</p>
                    </div>
                </section>

                <div class="people-section">
                  <?php $bg_bottom = get_field('background_image_bottom'); ?>
                  <img src="<?= $bg_bottom['url']; ?>" alt="<?= $bg_bottom['alt'].''.get_the_title(); ?>">
                    <div class="hover-elements">
                        <div class="d-flex align-items-start container justify-content-center">
                            <div data-aos="fade-right"  class="column-text white-font">
                                <h1>Our People</h1>
                                 <p>
                                    EXIS team consists of diligently selected professionals that join us based on:
                                    <ul>
                                    <li>their know-how,</li>
                                    <li>their experience,</li>
                                    <li>the love for their work, and</li>
                                    <li>their attitude.</li>
                                    <ul>
                                </p>
                            </div>
                            <div data-aos="fade-left" data-aos-delay="100" class="column-text white-font">
                                <h1>Our DNA</h1>
                        		<p>We embrace challenge, to deliver cost-efficient solutions adapted to your business</br></br>
We are proud of our deliverables and our results</br></br>
We are always eager to dive into novelty and emerging technologies</p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php get_template_part( 'page-templates/content', 'journey' ); ?>

                <?php get_template_part( 'page-templates/content', 'ecosystem' ); ?>

                <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
