<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

			<main class="container site-main" id="single-post-main">
                    <div class="fixed-menu">
                        <h5><?= __('Select Category', 'exis'); ?></h5>
                        <ul>
                            <li>
                                <a href="<?= get_permalink(get_option('page_for_posts')); ?>">
                                    <?= __('All', 'exis'); ?>
                                </a>
                            </li>
                            <?php
                            $terms = get_terms([
                                'taxonomy' => 'category',
                                'hide_empty' => false,
                                'exclude' => array(1)
                            ]);
                            foreach ($terms as $term) { ?>
                                <li <?php echo in_category($term->term_id) ? 'class="active-item"' : '' ?>>
                                    <a href="<?= get_category_link($term->term_id) ?>">
                                        <?= $term->name ?>
                                    </a>
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </div>

                <div class="single-post__inner">
                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'loop-templates/content', 'single' ); ?>
                        <h3 class="post-navigation__title"><?= __('Related Posts', 'exis')?></h3>
                        <?php understrap_post_nav(); ?>

                    <?php endwhile; // end of the loop. ?>


                </div>

			</main><!-- #main -->

	</div><!-- #content -->

</div><!-- #single-wrapper -->
<?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

<?php get_footer(); ?>
