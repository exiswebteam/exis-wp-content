<?php
/**
 *
 * Template Name: Projects Software
 */


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
$initProjectID = 32;
$url = esc_url( add_query_arg( 'client', $initProjectID , get_permalink() ));

if(!isset($_GET['client']) ){
    wp_redirect($url);
    exit;
}
get_header();

?>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="site-main" id="main">

                <?php get_template_part( 'page-templates/content', 'header-projects' ); ?>
                <div class="left-fixed-menu">
                    <h5><?= __('OUR CLIENTS', 'exis'); ?></h5>
                    <ul id="menu-projects">
                        <?php
                        //Term query args
                        $child_args = array(
                            'taxonomy'     => 'software_clients',
                            'orderby'      => 'name',
                            'order'        => 'ASC',
                            'hide_empty'   => false
                        );

                        //query
                        $children_query = new WP_Term_Query($child_args);

                        foreach($children_query->terms as $cat) {
                        ?>
                        <li class="logo-item<?php echo $cat->term_id === $initProjectID ? ' active-li' : ''?>">
                            <img src="<?= get_field('logo','term_'.$cat->term_id) ?>" alt="logo" data-project="<?= $cat->term_id ?>" <?php echo $cat->term_id === $initProjectID ? 'class="active-client"' : ''?>>
                        </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
                <?php
                $software_info = get_field('software_info', 127);
                ?>
                <section class="headline-section"<?= isset($_GET['client']) ? ' style="opacity: 0;" ' : '' ?>>
                    <div class="container">
                        <div class="row headline-section__inner">
                          <a href="#" class="mobile-dots">
                            <span class="dot"></span>
                            <span class="dot"></span>
                            <span class="dot"></span>
                          </a>
                          <header class="col-sm-10 col-md-8">
                            <h2 class="headline-section__title">

                            </h2>
                          </header>

                          <div class="col-sm-4">

                          </div>
                        </div>
                    </div>
                </section>

                <div class="other-projects">
                  <div class="other-projects__inner">
                      <section class="other-projects__label text-center">
                          <h5><?= __('CLIENT\'S PROJECTS', 'exis');?></h5>
                      </section>
                      <ul class="other-projects__list"></ul>
                  </div>
                </div>

                <section class="projects-container software-container"<?= isset($_GET['client']) ? ' style="opacity: 0;" ' : '' ?>>
                    <?php
                    $args = array(
                        'post_type' => 'software_projects',
                        'post_status' => 'publish',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'software_clients',
                                'field' => 'term_id',
                                'terms' => $initProjectID
                            )
                        ),
                        'orderby' => 'menu_order',
                        'order'   => 'ASC'
                    );

                    $ajaxposts = new WP_Query( $args );

                    if ( $ajaxposts->have_posts() ) {
                        while ( $ajaxposts->have_posts() ) {
                            $ajaxposts->the_post();
                            get_template_part( 'loop-templates/content', 'software' );
                        }
                    }
                    ?>
                </section>

                <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
