<?php
/**
 *
 * Template Name: List Customers filters
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>
<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="site-main" id="main">

                <?php get_template_part( 'page-templates/content', 'header-projects' ); ?>

                <section class="projects-container list-container">
                    <div class="container">
                        <div class="list-container__inner">
                            <h3 class="list-customers-title"><?php the_title(); ?></h3>
                            <div class="list-customers-desc">
                              <?php the_content(); ?>
                            </div>
                            <?php get_template_part( 'page-templates/list-customers/filters-menu' ); ?>
                            <?php get_template_part( 'page-templates/list-customers/filters-init' ); ?>
                        </div>
                    </div>
                </section>

                <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
