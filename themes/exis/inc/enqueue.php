<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}


/*====================================================================
   Understrap remove
====================================================================*/
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );
    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );
}

add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );


/*====================================================================
  Ajax_url
====================================================================*/
add_action('wp_footer', 'admin_ajaxurl');

function admin_ajaxurl()
{
   echo '<script type="text/javascript">';
   echo 'var EXIS_ajax_url = "' . admin_url('admin-ajax.php') . '";';
   echo '</script>';

   //Contact page - lets discuss - redirect script for cf7
   if(is_page(368))
   {
     echo "<script>";
     echo "document.addEventListener( 'wpcf7mailsent', function( event ) { location = '".get_permalink(1745)."'; }, false );";
     echo "</script>";
   }
}


/*====================================================================
   Styles and scripts
====================================================================*/
function theme_enqueue_styles() {
    // Get the theme data
    $the_theme = wp_get_theme();
    $version = $the_theme->get('Version');

    //Dequeue gutenberg & WP default styles
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wc-block-style' );
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    if(!is_single()){
      wp_deregister_script( 'wp-embed' );
    }


    //Dequeue contact form 7 and add with sass
    wp_deregister_style( 'contact-form-7' );

    //Exis style.css - Compiled with gulp
    wp_enqueue_style( 'exis', get_stylesheet_directory_uri() . '/style.css', array(), $version );

    //Gulp - changed to style css
    wp_enqueue_style( 'main-compiled', get_stylesheet_directory_uri() . '/dist/css/main.css', array('exis'), $version, false);

    //jQuery - deregister and use 2.2.4 from cdn
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js', array(), null, false);

    //Exis Scripts Compiled with gulp - gulpfire.js
    wp_enqueue_script( 'exis-script-gulp', get_stylesheet_directory_uri() . '/dist/js/main.js', array('jquery'), $version, true );


}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );


function add_preload_css()
{
    echo '<link rel="preload" href="'.get_stylesheet_directory_uri().'/dist/css/main.css'.'" as="style" type="font/woff2" crossorigin="anonymous">';
}
//add_action( 'wp_head', 'add_preload_css', 5);





/*====================================================================
   Contact form 7
====================================================================*/
function rjs_lwp_contactform_css_js()
{
    global $post;

    if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'contact-form-7') )
    {
        wp_enqueue_script('contact-form-7');
    }else{
        wp_dequeue_script( 'contact-form-7' );
    }
}
add_action( 'wp_enqueue_scripts', 'rjs_lwp_contactform_css_js');


/*====================================================================
   Google recaptcha
====================================================================*/
add_action('wp_print_scripts', function ()
{
	if ( !is_page( array( 'contact-us/','some-other-page-with-form' ) ) )
  {
		wp_dequeue_script( 'google-recaptcha' );
		wp_dequeue_script( 'google-invisible-recaptcha' );
	}
});


/*====================================================================
   REMOVE HEADER SCRIPTS - Clean head
====================================================================*/
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
remove_action('wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'index_rel_link' ); // index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0 ); // start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.


//Remove SVG css and added with SASS
remove_action( 'wp_enqueue_scripts', 'bodhi_svgs_frontend_css' );
