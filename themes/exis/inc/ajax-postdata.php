<?php
function get_post_data() {

    if ( !wp_verify_nonce( $_REQUEST['nonce'], "token")) {
        exit("no permission");
    }
    $catId = sanitize_key($_REQUEST['id']);
    // Query Arguments
    $args = array(
        'post_type' => array('software_projects'),
        'post_status' => array('publish'),
        'tax_query' => array(
            array(
                'taxonomy' => 'software_clients',
                'field' => 'term_id',
                'terms' => $catId
            )
        ),
        'orderby' => 'menu_order',
        'order'   => 'ASC'
    );

    // The Query
    $ajaxposts = new WP_Query( $args );
    $response = '';


    // The Query
    if ( $ajaxposts->have_posts() ) {
        while ( $ajaxposts->have_posts() ) {
            $ajaxposts->the_post();

              $response .=  get_template_part( 'loop-templates/content', 'software' );

        }
    }
    print $response;

    die();

}

add_action("wp_ajax_get_post_data", "get_post_data");
add_action("wp_ajax_nopriv_get_post_data", "get_post_data");
