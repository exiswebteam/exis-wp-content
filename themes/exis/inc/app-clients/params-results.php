<?php
//Tags - get params
$param_industries   = (isset($_GET['industries']))?urldecode($_GET['industries']): '';
$param_service   = (isset($_GET['type']))?urldecode($_GET['type']): '';


if(!empty($param_industries) || !empty($param_service)):
?>
<nav class="params-menu">
  <span class="found-results"><?= __('Filter by: ','exis'); ?></span>

<?php
/*  TAGS
--------------------------------------------------*/
if(!empty($param_industries))
{
  $array_industries = explode(",", $param_industries);

  if(count($array_industries))
  {
    foreach($array_industries as $term_id)
    {
      $terms_industry = get_term_by('term_id', $term_id, 'industries');

      echo '<span class="term-param">'.$terms_industry->name.'</span>';
    }
  }
}

if(!empty($param_service))
{
  $array_service = explode(",", $param_service);

  if(count($array_service))
  {
    foreach($array_service as $term_id)
    {
      $term_type = get_term_by('term_id', $term_id, 'type');

      echo '<span class="term-param">'.$term_type->name.'</span>';
    }
  }
}
?>

  <span class="clear-all">
    <a href="<?= get_permalink(); ?>"><?= __('Clear','exis'); ?> <i class="fa fa-times" aria-hidden="true"></i></a>
  </span>
</nav>

<?php
endif;
