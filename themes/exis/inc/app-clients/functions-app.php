<?php
/*-------------------------------------------------------------------------------------
       FN: params
-------------------------------------------------------------------------------------*/
function listcustomers_register_query_vars( $vars ) {
    $vars[] = 'industries';
    $vars[] = 'service';
    return $vars;
}

add_filter( 'query_vars', 'listcustomers_register_query_vars' );


/*-------------------------------------------------------------------------------------
       FN: Products filtering
-------------------------------------------------------------------------------------*/
function LIST_CUSTOMERS_filters_init($get_terms_params,$industries,$service)
{
  //Filters init
  if(!empty($get_terms_params)):

    //defaults vars - resets
    $build_tax_query = [];
    $ARRAY = [];

    /* WP QUERY ARGS - DEFAULTS */
    $post_type = 'clients';
    $posts_per_page = '-1';
    $order_by = 'name';
    $order = 'ASC';

    //Build query args for tags
    $ARRAY = $get_terms_params;
    $count_array = count($ARRAY);
    $merge_terms = array_merge($industries+$service);

    if($count_array >= 1)
    {
      foreach($merge_terms as $key => $value)
      {
        $build_tax_query[] = array( 'relation' => 'AND', array( 'taxonomy' => $key, 'field' => 'term_id', 'terms' => $value));
      }
    }

    /********** ARGS QUERY **********/
    $filters_aray = array(
      'post_type' => $post_type,
      'posts_per_page' => $posts_per_page,
      'ignore_sticky_posts'	=> 1,
      'post_status' => 'publish',
      'orderby' => $order_by,
      'order' => $order,
      'tax_query' => array(
        $build_tax_query
      )
    );

    //print_r($filters_aray);

    //WP query
    $list_customers_query = new WP_Query( $filters_aray );

    get_template_part('inc/app-clients/params-results');

    //Show Products
    if ( $list_customers_query->have_posts() )
    {
      echo '<div class="customer-wrap">';
    	while ( $list_customers_query->have_posts() )
      {
    		$list_customers_query->the_post();

        get_template_part('page-templates/list-customers/list-item');
    	}
      echo '</div>';

      wp_reset_postdata();
    }
    else
    {
      get_template_part('inc/app-clients/no-results');
    }
  endif;
}
