<?php
//Tags - get params
$param_industries   = (isset($_GET['industries']))? urldecode($_GET['industries']): '';
$param_service   = (isset($_GET['type']))? urldecode($_GET['type']): '';

if(!empty($param_industries) || !empty($param_service)):

  $industries = (!empty($param_industries))? array("industries" => explode(",", $param_industries)) : array();
  $service = (!empty($param_service))? array("type" => explode(",", $param_service)) : array();


   /***** FN: Filters init *****/
   $get_terms_params = array_values(array_merge($industries + $service));

   if( function_exists( 'LIST_CUSTOMERS_filters_init' ) )
   {
     LIST_CUSTOMERS_filters_init($get_terms_params,$industries,$service);
   }
   else
   {
     echo "<p>Oops...something went wrong. Please try again later or contact us.</p>";
   }

else:
  /***** Default Template *****/
  get_template_part('page-templates/list-customers/term-query');
endif;
?>
