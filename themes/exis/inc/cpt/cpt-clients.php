<?php
class clients_custom_post_type
{
    /* Construct
    ----------------------------------------------------------------------------------------*/
	public function __construct( )
	{
    //1. Post Type - Customers - list of customers
    add_action( 'init', array( $this, 'register_clients_cpt' ) );

		//2. custom taxonomy
    add_action( 'init', array( $this, 'register_clients_taxonomies' ) );

	}

  function register_clients_cpt()
  {
    $singular = 'Customers';
    $singular_lowercase = 'Customers';
    $plural = 'Customers®';
    $slug = 'customers';
    $post_type = 'clients';
    $desc = 'Customers';
    $supports = array('title','editor','thumbnail');

	  //Labels
    $labels = array(
    'name' => _x( $plural, 'post type general name'),
    'singular_name' => _x( $singular, 'post type singular name'),
    'add_new' => _x('Add New', $singular_lowercase ),
    'add_new_item' => __('Add New '. $singular_lowercase),
    'edit_item' => __('Edit '. $singular_lowercase ),
    'new_item' => __('New '. $singular_lowercase ),
    'view_item' => __('View '. $singular_lowercase),
    'search_items' => __('Search '. $plural),
    'not_found' =>  __('No '. $singular .' found'),
    'not_found_in_trash' => __('No '. $singular .' found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => $plural
    );

    //Args
    $args = array(
    'labels' => $labels,
    'description' => $desc,
    'public' => true,
    'menu_position' => 21,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'show_in_rest' => true,
		'rest_base' => 'clients',
    'rest_controller_class' => 'WP_REST_Posts_Controller',
    'query_var' => true,
    'menu_icon' => 'dashicons-businessman',
    'rewrite' => array('slug'=> $slug, 'with_front' => true),
    'capability_type' => 'post',
    'has_archive' => false,
    'exclude_from_search' => false,
    'hierarchical' => false,
    'supports' => $supports
    );

    register_post_type( $post_type, $args );
  }


	/* TAXONOMY */
  function register_clients_taxonomies()
  {

    //Taxonomies Properties
      $taxonomies = array(
        array(
          'slug'         => 'industries',
          'single_name'  => 'Industry',
          'plural_name'  => 'Industries',
          'post_type'    => array('clients'),
          'hierarchical' => true,
          'rewrite'      => array( 'slug' => 'industry', 'with_front' => true),
        ),
				array(
          'slug'         => 'type',
          'single_name'  => 'EXIS® Service',
          'plural_name'  => 'EXIS® Services',
          'post_type'    => array('clients'),
          'hierarchical' => true,
          'rewrite'      => array( 'slug' => 'exis-service', 'with_front' => true),
        ),
      );
      //Register
      foreach( $taxonomies as $taxonomy )
      {
        $labels = array(
          'name' => $taxonomy['plural_name'],
          'singular_name' => $taxonomy['single_name'],
          'search_items' =>  'Search ' . $taxonomy['plural_name'],
          'all_items' => 'All ' . $taxonomy['plural_name'],
          'parent_item' => 'Parent ' . $taxonomy['single_name'],
          'parent_item_colon' => 'Parent ' . $taxonomy['single_name'] . ':',
          'edit_item' => 'Edit ' . $taxonomy['single_name'],
          'update_item' => 'Update ' . $taxonomy['single_name'],
          'add_new_item' => 'Add New ' . $taxonomy['single_name'],
          'new_item_name' => 'New ' . $taxonomy['single_name'] . ' Name',
          'menu_name' => $taxonomy['plural_name']
        );
        //For no errors
        $rewrite = isset( $taxonomy['rewrite'] ) ? $taxonomy['rewrite'] : array( 'slug' => $taxonomy['slug'] );
        $hierarchical = isset( $taxonomy['hierarchical'] ) ? $taxonomy['hierarchical'] : true;
        //Register
        register_taxonomy( $taxonomy['slug'], $taxonomy['post_type'], array(
          'hierarchical' => $hierarchical,
          'labels' => $labels,
          'show_ui' => true,
          'show_admin_column' => true,
          'query_var' => true,
          'rewrite' => $rewrite,
          'show_in_rest' => true,
          'show_in_nav_menus' => true,
        ));
     }
  }// END taxonomy


}//END Class

new clients_custom_post_type();
