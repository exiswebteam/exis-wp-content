<?php
class testimonials_custom_post_type
{
    /* Construct
    ----------------------------------------------------------------------------------------*/
	public function __construct( )
	{
    //1. Post Type - Customers - list of customers
    add_action( 'init', array( $this, 'register_testimonials_cpt' ) );
	}

  function register_testimonials_cpt()
  {
    $singular = 'Testimonials';
    $singular_lowercase = 'Testimonial';
    $plural = 'Testimonials®';
    $slug = 'clients-testimonials';
    $post_type = 'testimonials';
    $desc = 'Testimonials';
    $supports = array('title','editor','thumbnail');

	  //Labels
    $labels = array(
    'name' => _x( $plural, 'post type general name'),
    'singular_name' => _x( $singular, 'post type singular name'),
    'add_new' => _x('Add New', $singular_lowercase ),
    'add_new_item' => __('Add New '. $singular_lowercase),
    'edit_item' => __('Edit '. $singular_lowercase ),
    'new_item' => __('New '. $singular_lowercase ),
    'view_item' => __('View '. $singular_lowercase),
    'search_items' => __('Search '. $plural),
    'not_found' =>  __('No '. $singular .' found'),
    'not_found_in_trash' => __('No '. $singular .' found in Trash'),
    'parent_item_colon' => '',
    'menu_name' => $plural
    );

    //Args
    $args = array(
    'labels' => $labels,
    'description' => $desc,
    'public' => true,
    'menu_position' => 21,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'show_in_nav_menus' => true,
    'show_in_rest' => false,
    'query_var' => true,
    'menu_icon' => 'dashicons-format-chat',
    'rewrite' => array('slug'=> $slug, 'with_front' => true),
    'capability_type' => 'post',
    'has_archive' => false,
    'exclude_from_search' => false,
    'hierarchical' => false,
    'supports' => $supports
    );

    register_post_type( $post_type, $args );
  }

}//END Class

new testimonials_custom_post_type();
