<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'EXIS® Theme',
		'menu_title'	=> 'EXIS® Theme',
		'menu_slug' 	=> 'exis-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

  acf_add_options_sub_page(array(
		'page_title' 	=> 'Counters',
		'menu_title'	=> 'Counters',
    'menu_slug' 	=> 'general-settings',
		'parent_slug'	=> 'exis-general-settings',
    'capability'	=> 'edit_posts',
    'redirect'		=> false
	));
}


/*==============================================================
    1. Register - CSS & JS within Admin area
================================================================*/
add_action('admin_enqueue_scripts', 'admin_style');

function admin_style()
{
  if(class_exists('acf'))
  {
    $the_theme = wp_get_theme();

    wp_enqueue_style('acf-admin-styles', get_stylesheet_directory_uri().'/inc/acf/admin.css');
  }
}

 /*==============================================================
     2. Hide ACF Admin on Staging/Production
 ================================================================*/
function awesome_acf_hide_acf_admin() {

    // get the current site url
    $site_url = get_bloginfo( 'url' );

    // an array of protected site urls
    $protected_urls = array(
        'https://exis.com.gr', //production
        'http://exis.com.gr'
    );

    // check if the current site url is in the protected urls array
    if ( in_array( $site_url, $protected_urls ) ) {

        // hide the acf menu item
        return false;

    } else {

        // show the acf menu item
        return true;

    }

}

add_filter('acf/settings/show_admin', 'awesome_acf_hide_acf_admin');
