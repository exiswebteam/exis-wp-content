<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

function wpb_widgets_init() {

    register_sidebar( array(
        'name'          => 'Footer column 1',
        'id'            => 'footer-column-1',
        'before_widget' => '<div id="%1$s" class="foot-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="foot-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer column 2',
        'id'            => 'footer-column-2',
        'before_widget' => '<div id="%1$s" class="foot-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="foot-title">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => 'Footer column 3',
        'id'            => 'footer-column-3',
        'before_widget' => '<div id="%1$s" class="foot-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="foot-title">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => 'Footer column 4',
        'id'            => 'footer-column-4',
        'before_widget' => '<div id="%1$s" class="foot-widget">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="foot-title">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'wpb_widgets_init' );
