<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article class="blog-item">
    <a href="<?= get_permalink(); ?>">
        <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
    </a>

    <header class="blog-entry-header">
			<?php
			the_title(
					sprintf( '<h2 class="blog-entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
					'</a></h2>'
			);
			?>
			<div class="post-info-wrap">
				<time class="blog-date"><?= get_the_date(); ?></time>
	      <p class="blog-excerpt"><?= exis_excerpt(25);?></p>
				<a href="<?= get_permalink(); ?>"><u>View more</u></a>
			</div>

    </header>
</article>
