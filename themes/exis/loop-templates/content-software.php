<?php
$software_info = get_field('software_info');
?>
<div class="software-container-loop" data-id="<?= get_the_ID(); ?>" data-title="<?= get_the_title(); ?>">
    <div class='container'>
        <div class='row software-container__inner'>
            <div class='col-md-6 d-flex project-info-box'>
                <div class='project-info__label'>
                    <div class='project-info__label--div'>
                        <?= __('Client', 'exis'); ?>
                    </div>
                </div>
                <div class='project-info__desc'>
                    <div class='project-info__client-desc'>
                        <?php
                        $category = get_the_terms( $post->ID, 'software_clients' );
                        $termID = $category[0]->term_id;
                        $termDesc = $category[0]->description;

                        echo $termDesc;
                       ?>
                    </div>
                </div>
            </div>
            <div class='col-md-6 text-center logo-client-section'>

                <img class='thumbnail-client' src='<?= get_field('logo', 'term_'.$termID); ?>' alt=''>
            </div>

        </div>
    </div>
    <div class='grey-bg'>
        <div class='container'>
            <div class='row software-container__inner'>
                <div class='col-md-6 d-flex project-info-box'>
                    <div class='project-info__label'>
                        <div class='project-info__label--div'>

                            <?= __('Need', 'exis'); ?>
                        </div>
                    </div>
                    <div class='project-info__desc'>
                        <div class='project-info__need-desc'>
                            <?php echo $software_info['need']; ?>
                        </div>
                    </div>
                </div>
                <div class='col-md-6 d-flex project-info-box'>
                    <div class='project-info__label'>
                        <div class='project-info__label--div'>

                            <?= __('Approach', 'exis'); ?>
                        </div>
                    </div>
                    <div class='project-info__desc'>
                        <div class='project-info__approach-desc'>
                            <?php echo $software_info['approach']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class='solution-section'>
        <div class='container'>
            <div class='row software-container__inner'>
                <div class='col-md-11 d-flex project-info-box'>
                    <div class='project-info__label'>
                        <div class='project-info__label--div'>
                            <?= __('Solution', 'exis'); ?>
                        </div>
                    </div>
                    <div class='project-info__desc'>
                        <div class='project-info__solution-desc'>
                            <?php echo $software_info['solution']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='outcome-section'>
        <div class='container'>
          <div class='row software-container__inner'>
              <div class='project-info__label'>
                <div>
                    <h2 class="title-outcome">
                        <?= __('Outcome <br/> & Benefit', 'exis'); ?>
                    </h2>
                </div>
              </div>

              <div class='col-md-11 d-flex project-info-box'>
                  <div class='project-info__desc'>
                      <div>
                          <?php echo $software_info['outcome_benefit']; ?>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>

    <div class='container technologies-section'>
        <div class='text-center technologies-section__inner'>
            <h5><?= __('Technologies used'); ?></h5>
            <div class='technologies-section__list'>
                <ul>
                    <?php
                    $technologies = get_field('technologies');
                    ?>
                    <?php
                    foreach($technologies as $technology) {
                        ?>
                        <li><?php echo $technology['tech_name']; ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
