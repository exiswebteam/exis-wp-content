<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="single-entry-header">

		<?php the_title( '<h1 class="single-entry-title">', '</h1>' ); ?>
		<time class="blog-date"><?= get_the_date(); ?></time>

	</header><!-- .entry-header -->
    <div class="single-thumbnail">
        <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
    </div>

	<div class="entry-content">

		<?php the_content(); ?>

	</div><!-- .entry-content -->

</article><!-- #post-## -->
