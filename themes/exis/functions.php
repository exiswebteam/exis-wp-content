<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/*====================================================================
   Includes - Set up theme
====================================================================*/
$child_theme_includes = array(
    'inc/enqueue.php',
    'inc/widgets.php',
    'inc/app-clients/functions-app.php',
    'inc/cpt/cpt-credentials.php',
    'inc/cpt/cpt-testimonials.php',
    'inc/cpt/cpt-clients.php',
    'inc/cpt/cpt-consulting.php',
    'inc/cpt/cpt-services.php',
    'inc/cpt/cpt-software.php',
    'inc/cpt/cpt-web.php',
    'inc/cpt/cpt-journey.php',
    'inc/acf/acf-setup.php',
    'inc/ajax-postdata.php'
);

//Inclusions
foreach ($child_theme_includes as $file)
{
    if (!$filepath = locate_template($file))
    {
        trigger_error(sprintf(__('Error locating file for inclusion'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);


/*====================================================================
  Excerpt limit and content
====================================================================*/
function exis_excerpt($limit)
{
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}

function exis_content($limit)
{
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }
  $content = preg_replace('/[.+]/','', $content);
  $content = apply_filters('the_content', $content);
  $content = str_replace(']]>', ']]>', $content);
  return $content;
}


/*====================================================================
   Pre get posts custom fn
====================================================================*/
function wpb_custom_query( $query )
{
  //For Taxonomy web_categories
  if( $query->is_main_query() && ($query->is_tax('web_categories')) )
  {
    //By Date
    //$query->set( 'orderby', 'date' );
    //$query->set( 'order', 'DESC' );

    //By Menu order
    $query->set( 'orderby', 'menu_order' );
    $query->set( 'order', 'ASC' );
    $query->set( 'posts_per_page', '-1' );
  }
}

add_action( 'pre_get_posts', 'wpb_custom_query' );
