<?php
/**
 *
 * Template Name: What We Do
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="site-main" id="main">

                    <?php get_template_part( 'page-templates/content', 'page-header-what' ); ?>

                    <?php get_template_part( 'page-templates/content', 'what' ); ?>

                    <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>


            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
