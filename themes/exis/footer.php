<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<section class="vertical-scrolling fp-auto-height wrapper" id="wrapper-footer">

	<div class="container-fluid">

		<div class="row">

			<div class="col-md-12">

				<?php
				get_template_part( 'page-templates/content-footer/content', 'widgets' );

				get_template_part( 'page-templates/content-footer/content', 'bottom' );
				?>

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</section><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>
