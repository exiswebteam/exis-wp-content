<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="front-wrapper">

	<div id="content" tabindex="-1">

		<main class="site-main" id="front-main">
			<?php  get_template_part( 'page-templates/content-frontpage/content', 'fold' ); ?>

			<?php get_template_part( 'page-templates/content-frontpage/content', 'clients' ); ?>

			<?php get_template_part( 'page-templates/content-frontpage/content', 'services' ); ?>

			<?php get_template_part( 'page-templates/content', 'what' ); ?>

			<?php
			if(!wp_is_mobile())
			{
				get_template_part( 'page-templates/content-frontpage/content', 'verticalSlider' );
			}
			?>

			<?php get_template_part( 'page-templates/content-frontpage/content', 'partners' ); ?>

			<?php get_template_part( 'page-templates/content-frontpage/content', 'technologies' ); ?>

			<?php get_template_part( 'page-templates/content-frontpage/content', 'news-cta' ); ?>

		</main><!-- #main -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->


<?php get_footer(); ?>
