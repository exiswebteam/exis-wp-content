/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */
(function($)
{
  var EXIS_JS = {
    // All pages
    'common': {
      init: function()
      {
        //AOS initialize
        AOS.init();

        var $body = $('body');
        var $window = $(window);
        var $preloader = $('#overlay');
        var $timelineNav = $('#timeline-nav');

        //Loader - Giannis
        $(window).on('load', function()
        {
          if($preloader.length)
          {
            $preloader.fadeOut();
          }

          //Chris
          $body.addClass('loaded');

          /* FN: Sticky menu  - single web projects
          ----------------------------------------------------*/
          $('#sticky-menu').stickyMenu();


          if($timelineNav.length)
          {
            /* FN: Scroll steps and Fixed Nav
            ----------------------------------------------------*/
            $(".scroll-nav-section").Timeline_scrollSections();

            //*** Active class on scroll
            $timelineNav.Timeline_activeClass();

            //*** hashtagScroll
            $window.Timeline_hashtagScroll();

            //*** Scrolling events
            $timelineNav.Timeline_NavScrollingEvent();

            //*** Timeline nav show
            $timelineNav.Timeline_headerScrollingFN();
          }
        });


        /* FN: Header - Chris
        ------------------------------------------------*/
        $('#wrapper-navbar').headerScrollingFN();
        $('.hamburger').toggleMobileNav();

        if(!$('#wpfront-notification-bar-spacer').length)
        {
          $body.scrollUpandDownHeader();
        } else {
          $body.addClass('wpfront-bar-on');
        }


        /* FN: Footer Widgets toggle on mobile - Chris
        ------------------------------------------------*/
        $('.foot-widget').EXIS_footerNavToggle();

        /* FN: Giannis sidebar
        ------------------------------------------------*/
        $window.sidebarHideOnScroll();

        /****  List account filters
        --------------------------------------------------------------------*/
        $('.wrap-filters-menus').searchFiltersEvents();
        $('.page-template-list-customers-filters').filters_window_change();
      },
      finalize: function()
      {
        var $body = $('body');
        var $projects_logos = $('.logo-item');

        $body.find('.unclick').click(function(e){e.preventDefault();});

        /* FN: Slideshows: Giannis
        ------------------------------------------------*/
        $('.news-slider').NEWS_slickSlider();
        $('.slider-client').CLIENTS_slickSlider();
        $('.slider-partners').PARTNERS_slickSlider();
        $('.services-slider').SERVICES_slickSlider();

        /* FN: What section: Giannis
        ------------------------------------------------*/
        $('.section-what-area-wrap').whatSection();

        /* FN: System inegration Giannis slideshow
        ------------------------------------------------*/
        $('.system-integration__right').systemIntegration();

        /* FN: Categories List - Giannis
        ------------------------------------------------*/
        $(".projects-categories__list").categoriesList();

        /* FN: Scroll top Giannis
        ------------------------------------------------*/
        $('.scroll-top').scrollToTopFN();

        /* FN: List customers - Giannis
        ------------------------------------------------*/
        $('.list-customers-menu ul li').listCustomers();

        /* FN: Event call for ajax Projects - Giannis
        ------------------------------------------------*/
        $projects_logos.ajaxProjectsOnPageLoad(); //init
        $projects_logos.ajaxProjectsmenuClickEvent();

        /* FN: Visible element - chris
        ----------------------------------------------------*/
        $body.find('.visible-el').visibleElementFN();

        /* FN: Parallax - chris
        ----------------------------------------------------*/
        $('.parallax-init').EXIS_parallax();
      }
    },
    'home': {
      init: function()
      {
        /* Home animations
        ---------------------------------------------*/
        $(window).homeAnimationsInit();
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = EXIS_JS;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  /* LOADING ALL EVENTS HERE */
  $(document).ready(UTIL.loadEvents);




/*----------------------------------------------------------------------------
  Callback var: Get params - giannis
----------------------------------------------------------------------------*/
var getParameterByName = function(name, url)
{
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
};


/*----------------------------------------------------------------------------
  Global: isInViewport - for fn EXIS_parallax
----------------------------------------------------------------------------*/
var isInViewport = function(node) {
  var rect = node.getBoundingClientRect();
  return (
    (rect.height > 0 || rect.width > 0) &&
    rect.bottom >= 0 &&
    rect.right >= 0 &&
    rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.left <= (window.innerWidth || document.documentElement.clientWidth)
  );
};



/*----------------------------------------------------------------------------------
                      FN: STICKY MENU
-----------------------------------------------------------------------------------*/
$.fn.stickyMenu = function()
{
  var $sticky = $(this);
  var $window = $(window);
  var $stickyrStopper = $('.outcome-single-row');

  if (!!$sticky.offset())
  {
    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stopperOffset = 100;
    var stickyStopperPosition = $stickyrStopper.offset().top - stopperOffset;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $window.scroll(function()
    {
      if($window.width()>=1199)
      {
        var windowTop = $window.scrollTop(); // returns number

        if (stopPoint < windowTop)
        {
          $sticky.css({ position: 'absolute', top: '0'/*diff*/ });
        }
        else if (stickyTop < windowTop+stickOffset)
        {
          $sticky.css({ position: 'fixed', top: stickOffset });
        }
        else
        {
          $sticky.css({position: 'absolute', top: '0'});
        }
      }

      if($window.width()<1199)
      {
        $sticky.css({position: 'absolute', top: 'initial'});
      }
    });
  }
};



/*----------------------------------------------------------------------------
  FN: Parallax jQuery
----------------------------------------------------------------------------*/
$.fn.EXIS_footerNavToggle = function()
{
  var $target = $(this);

  if($target.length)
  {
    if($(window).width() < 768)
    {
      $target.find('.foot-title').click(function()
      {
        var $wrapper = $(this).parents('.foot-widget').eq(0);
        var $ul = $wrapper.find('ul');

        if($ul.is(':visible'))
        {
          $ul.slideUp();
          $wrapper.removeClass('active');
        } else {
          $ul.slideDown();
          $wrapper.addClass('active');
        }
        return false;
      });
    }

  }
};


/*----------------------------------------------------------------------------
  FN: Parallax jQuery
----------------------------------------------------------------------------*/
$.fn.EXIS_parallax = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window);

    $window.scroll(function()
    {
      var scrolled = $window.scrollTop();

      $target.each(function(index, element)
      {
        var property = $(this).attr('data-property');
        var initY = $(this).offset().top;
        var height = $(this).height();
        var endY  = initY + $(this).height();

        var visible = isInViewport(this);

        if(visible)
        {
          var diff = scrolled - initY;
          var ratio = Math.round((diff / height) * 100);
          $(this).css(property,parseInt(-(ratio * 0.3)) + 'px');
        }
      });
    });
  }
};


/*----------------------------------------------------------------------------
  FN: LIST CUSTOMERS - Giannis
----------------------------------------------------------------------------*/
$.fn.listCustomers = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.click(function ()
    {
      var letter = $(this).data('letter');

      $('html, body').animate({
        scrollTop: $("#row-"+letter).offset().top - 80
      }, 500);
    });
  }
};



/*----------------------------------------------------------------------------
  FN: Scroll top giannis
----------------------------------------------------------------------------*/
$.fn.scrollToTopFN = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $left_menu = $('.left-fixed-menu');
    var $window = $(window);

    $target.click(function(e)
  	{
  		e.preventDefault();

  		$("html, body").animate({scrollTop: 0}, 600);
  	});

  	$window.scroll(function()
    {
  		if ($(this).scrollTop() >= 100)
      {
  			$target.css('opacity', '1');
  		} else {
  		 	$target.css('opacity', '0');
      }
  	});


    if($left_menu.length)
    {
      $window.scroll(function()
      {

        var width = window.innerWidth;

        if ($(this).scrollTop() >= 400 && width >= 800)
        {
          $left_menu.addClass('fixed');
        } else{
          $left_menu.removeClass('fixed');
        }
    	});
    }
  }
};



/*----------------------------------------------------------------------------
  FN: Projects AJAX on every page load if client=id exist - Giannis logic
----------------------------------------------------------------------------*/
$.fn.ajaxProjectsOnPageLoad = function()
{
  var $target = $(this);

  if($target.length)
  {
    //INIT
    var _url = window.location.href;
    var client = getParameterByName('client',_url);

    if(client != null)
    {
      var parent = $target.find('img[data-project="'+client+'"]').parent('.logo-item');

      $target.find('img').removeClass('active-client');
      $target.removeClass('active-li');

      $target.find('img[data-project="'+client+'"]').addClass('active-client');

      parent.addClass('active-li');

      $(window).projectsAJAX_CALL(parent,client);
    }
  }

};


/*----------------------------------------------------------------------------
  FN: Projects Click event for AJAX left menu - Giannis logic
----------------------------------------------------------------------------*/
$.fn.ajaxProjectsmenuClickEvent = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.find('img').click(function ()
    {
      if($(this).hasClass('active-client')){
        return;
      }

      $('.logo-item img').removeClass('active-client');
      $(this).addClass('active-client');

      $('.other-projects').removeClass('active');

      var _id= $(this).data('project');
      var parent = $(this).parent('.logo-item');

      window.history.pushState(null,null, '?client='+_id+'');

      $('.logo-item').removeClass('active-li');
      parent.addClass('active-li');

      $(this).projectsAJAX_CALL(parent,_id);
    });
  } //END if target
};



/*----------------------------------------------------------------------------
  FN: AJAX Projects - Giannis
----------------------------------------------------------------------------*/
$.fn.projectsAJAX_CALL = function(parent,_id)
{
  var nonce = $('meta[name=token]').attr("content");
  var width = window.innerWidth;
  var $containers = $('.projects-container,.headline-section');

  $containers.removeClass('loaded');

  if(width >=800)
  {
    var scroll = $('.left-fixed-menu>ul').scrollTop();

    $(".left-fixed-menu>ul").animate({ scrollTop: parent.position().top - $('.left-fixed-menu>ul li:first').position().top}, 600);
  } else {
    $(".left-fixed-menu>ul").animate({ scrollLeft:  parent.position().left - $('.left-fixed-menu>ul li:first').position().left }, 600);
  }

  $.ajax({
    url: EXIS_ajax_url,
    type : 'post',
    dataType : "html",
    data : {
      action : 'get_post_data',
      id: _id,
      nonce: nonce
    },
    complete: function(data)
    {
      //DATA html
      $('.software-container').html(data.responseText);

      //Callback
      $('.other-projects').renderProject();

      //Event callback
      $('.other-projects__list').otherProjectsEvent();

      //Callback
      $containers.addClass('loaded');

      //Scrolling effect
      //$("html, body").animate({scrollTop:$('.headline-section').position().top - 160}, 600);
    }
  });
};


/*----------------------------------------------------------------------------
  Callback: Render - Giannis
----------------------------------------------------------------------------*/
$.fn.renderProject = function()
{
  var $target = $(this);

  if($target.length)
  {
    var list = '';
    var $other_projects = $target;
    var $software_loop = $( '.software-container-loop' );
    var $other_projects_list = $('.other-projects__list');
    var $dropdown_ul = $('#menu-projects > .active-li').find('.dropdown-menu-clients > ul');
    var $other_menu = $('.mobile-dots');

    //Url
    var url = window.location.href;
    var project = getParameterByName('project',url);
    var title = '';

    //Html links
    $software_loop.each(function()
    {
      var postsID = $( this ).data( "id" );
      var titlePost = $( this ).data( "title" );
      list += '<li data-show='+postsID+'>'+titlePost+'</li>';
    });


    $other_projects_list.html(list).promise().done(function()
    {
      /* Callback
      ------------------------------------------------------*/
      $('.other-projects__list').otherProjectsEvent();
    });

    if(project != null)
    {
      title = $('.software-container-loop[data-id="'+project+'"]').data('title');
      $software_loop.fadeOut(100);
      $('.software-container-loop[data-id="'+project+'"]').fadeIn(100);

      $('ul.other-projects__list li[data-show="'+project+'"]').addClass('active-project-li');
      $('.headline-section__title').html(title);
    }
    else
    {
      title = $('.software-container-loop:first-child').data('title');
      $('ul.other-projects__list li:first-child').addClass('active-project-li');
      $('.headline-section__title').html(title);
    }


    if($software_loop.length > 1)
    {
      $other_projects.addClass('has-projects');
      $other_menu.addClass('has-menu');
    } else {
      $other_projects.removeClass('has-projects');
      $other_menu.removeClass('has-menu');

      if($(window).width() < 998)
      {
        $other_projects.hide();
      }
    }
  } //END if target
};



/*----------------------------------------------------------------------------
  FN: othrer Projects - Giannis
----------------------------------------------------------------------------*/
$.fn.otherProjectsEvent = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.off();
    $target.on('click','li',{},function(e)
    {
      e.preventDefault();

  		var _id = $(this).data('show');
      var _url = window.location.href.split("&");
  		var item = $(this);
      var $wrapper_ul = $(this).parents('.other-projects__list').eq(0);

      //Push history
  		window.history.pushState(null,null, _url[0]+'&project='+_id);

  		if(!$('.software-container-loop[data-id="'+_id+'"]').is(':visible'))
      {
        var $containers = $('.projects-container, .headline-section');
        var _title = $('.software-container-loop[data-id="'+_id+'"]').data('title');

        //Classes
  			$wrapper_ul.find('li').removeClass('active-project-li');
  			item.addClass('active-project-li');

        //loading
  			$containers.removeClass('loaded');

        //Fade out callback - when fade is done
        $('.software-container-loop').fadeOut().promise().done(function()
        {
          $('.headline-section__title').html(_title);

          $('.software-container-loop[data-id="'+_id+'"]').fadeIn().promise().done(function()
          {
            setTimeout(function ()
            {
              $containers.addClass('loaded');
            },300);
          });
        });

        //Toggle
        if($wrapper_ul.hasClass('active'))
        {
          $wrapper_ul.removeClass('active');

        } else {
          $wrapper_ul.addClass('active');
        }
  		}

      return false;
  	});
  }

  $('.headline-section__inner').off();

  $('.headline-section__inner').on('click','.mobile-dots',{},function(e)
  {
    e.preventDefault();

    var $wrapper_menu = $('.other-projects');

    if($wrapper_menu.is(':visible'))
    {
      $(this).removeClass('active');
      $wrapper_menu.slideUp();
      $wrapper_menu.removeClass('active');
    } else {
      $(this).addClass('active');
      $wrapper_menu.addClass('active');
      $wrapper_menu.slideDown();
    }


    return false;
  });
};


/*----------------------------------------------------------------------------
  FN: Categories list
----------------------------------------------------------------------------*/
$.fn.categoriesList = function()
{
  var $target = $(this);

  if($target.length)
  {
    var width = window.innerWidth;

    if(width<=800)
  	{
  		$target.animate({ scrollLeft:  $('.active.projects-categories__list-item').position().left - $('.projects-categories__list li:first').position().left }, 600);
  	}
  }
};


/*----------------------------------------------------------------------------
  FN: Sidebar hide on scroll - Giannis
----------------------------------------------------------------------------*/
$.fn.sidebarHideOnScroll = function()
{
  var $target = $(this);
  var $menu_projects = $('.other-projects');
  var $menu_news = $('.fixed-menu');
  var $document = $(document);
  var $window = $(window);

  /* make blog sidebar transparent when reaching footer */
  if($menu_news.length)
  {
    $target.scroll(function()
    {
    var threshold = 350; // number of pixels before bottom of page that you want to start fading
    var op = (($document.height() - $window.height()) - $window.scrollTop()) / threshold;
    if( op <= 0 )
    {
      $menu_news.hide();
    } else {
      $menu_news.show();
    }
      $menu_news.css("opacity", op );
    });
  }

  /* Projects */
  if($menu_projects.length)
  {
    /* make projects sidebar transparent when reaching footer */
    $target.scroll(function()
    {
      var threshold = 700; // number of pixels before bottom of page that you want to start fading
      var op = (($document.height() - $window.height()) - $window.scrollTop()) / threshold;

      if( op <= 0 )
      {
        //$menu_projects.hide();
      } else {
        //$menu_projects.show();
      }
      $menu_projects.css("opacity", op );
    });
  }
};



/*----------------------------------------------------------------------------
  FN: System Integration
----------------------------------------------------------------------------*/
$.fn.systemIntegration = function()
{
  var $target = $(this);

  if($target.length)
  {
    var slider = document.querySelector('.system-integration__right');
    var isDown = false;
    var startX;
    var scrollLeft;

    slider.addEventListener('mousedown', function (e) {
        isDown = true;
        slider.classList.add('active');
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener('mouseleave', function () {
        isDown = false;
        slider.classList.remove('active');
    });
    slider.addEventListener('mouseup', function () {
        isDown = false;
        slider.classList.remove('active');
    });
    slider.addEventListener('mousemove', function (e) {
        if (!isDown) return;
        e.preventDefault();
        var x = e.pageX - slider.offsetLeft;
        var walk = (x - startX) * 1; //scroll-fast

        slider.scrollLeft = scrollLeft - walk;
    });

    var pageContent = document.querySelector('.page-content-section');
    var offsetLeft = pageContent.offsetLeft;

    //init
    $('#system-integration, #our-process, #areas-expertise__inner').css('margin-left', offsetLeft);

    //Arrow event
    $('.system-intergration__arrow--right > a, .arrow-right-black').click(function(e)
    {
      e.preventDefault();

      var itemWidth = $('.system-integration__item').outerWidth();
      var scroll = $('.system-integration__right').scrollLeft() + itemWidth;

      $('.system-integration__right').animate({ scrollLeft: scroll }, 600);

      return false;
    });

    //Arrow event
    $('.system-intergration__arrow--left > a, .arrow-left-black').click(function(e)
    {
      e.preventDefault();

      var itemWidth = $('.system-integration__item').outerWidth();
      var scroll = $('.system-integration__right').scrollLeft() - itemWidth;

       $('.system-integration__right').animate({ scrollLeft: scroll }, 600);

       return false;
    });
  }
};


/*----------------------------------------------------------------------------
  FN: HOME ANIMATIONS
----------------------------------------------------------------------------*/
$.fn.homeAnimationsInit = function()
{
  var width = window.innerWidth;
  var controller = new ScrollMagic.Controller();

  if(width >=1200)
  {
    new ScrollMagic.Scene({
      triggerElement: ".front-clients-section",
      duration: '100%'
    })
    .setClassToggle(".front-video-section", "is-not-active")
    .addTo(controller);

    new ScrollMagic.Scene({
      triggerElement: ".front-clients-section",
      duration: '150%'
    })
    .setClassToggle(".front-clients-section", "is-active")
    .addTo(controller);

    new ScrollMagic.Scene({
      triggerElement: ".front-services-section",
      duration: '150%'
    })
    .setClassToggle(".front-services-section", "is-active")
    .addTo(controller);

    new ScrollMagic.Scene({
      triggerElement: ".front-services-section",
      duration: '150%'
    })
    .setClassToggle(".front-services-wrapper-text", "is-active")
    .addTo(controller);

    new ScrollMagic.Scene({
      triggerElement: ".services-slider-section",
      duration: '100%'
    })
    .setClassToggle(".front-services-text-filter", "is-active")
    .addTo(controller);
  }
};




/*-------------------------------------------------------------------------------
    Toggle Button
-------------------------------------------------------------------------------*/
$.fn.toggleMobileNav = function()
{
  //Target body
  var $target = $(this);

  if($target.length)
  {
    var $dropdownNav = $('#navbarNavDropdown');
    var $body = $('body');

    //Open
    $target.click(function(e)
    {
      e.preventDefault();

      if($body.hasClass('opened-menu'))
      {
        $body.removeClass('opened-menu');
      }
      else
      {
        $body.addClass('opened-menu');
      }

      return false;
    });

    //Close
    $('.mobile-nav-wrap .close-item').click(function(e)
    {
      e.preventDefault();

      if($body.hasClass('opened-menu'))
      {
        $body.removeClass('opened-menu');
      }
      else
      {
        $body.addClass('opened-menu');
      }

      return false;
    });
  }
};



/*-------------------------------------------------------------------------------
    Scroll Up and Down - Header Hide and show
-------------------------------------------------------------------------------*/
$.fn.scrollUpandDownHeader = function()
{
  //Target body
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window);
    var $headerToHide = $('.navbar');


    //Handle scrolling if bigger than 1000
    if($window.width() > 320)
    {
      //Reset vars
      var lastScrollTop       = 0,
          initTop             = 0,
          changeDirection     = false,
          gutterOffset        = 200,
          lastDirectionDown   = false;

      //Height
      var headerHeight = 81; //$headerToHide.height();

      $window.scroll(function ()
      {
        if(!$('#navbarNavDropdown').hasClass('show'))
        {
          var thisScrollTop = $(this).scrollTop();
              changeDirection = ( thisScrollTop > gutterOffset && (thisScrollTop > lastScrollTop && lastDirectionDown === false) || (thisScrollTop < lastScrollTop && lastDirectionDown === true) );

          if (changeDirection === true)
          {
            //Class for css if needed
            $headerToHide.toggleClass('scrolling-header');

            lastDirectionDown = ( lastDirectionDown === false );
          }

          $headerToHide.css( {
            'top': $headerToHide.hasClass('scrolling-header') ? (-1) * headerHeight : initTop
          });

          lastScrollTop = thisScrollTop;
        }
      });
    }
  }
};



/*-------------------------------------------------------------------------------
    Scrolling class
-------------------------------------------------------------------------------*/
$.fn.headerScrollingFN = function()
{
  var $target = $(this);

  if($target.length)
  {
    var _height = $target.height()+300,
        _projectsheight = $target.height()+400,
        $body = $("body"),
        $window = $(window),
        $pos = "";

    $window.scroll(function()
    {
       $pos = $window.scrollTop();

       //for header
       if($pos>=_height)
       {
         $body.addClass("scrolling-init");
       } else {
         $body.removeClass("scrolling-init");
       }

       //For projects
       if($pos>=_projectsheight)
       {
         $body.addClass("show-sidemenu");
       } else {
         $body.removeClass("show-sidemenu");
       }
    });
  }
};



/*-------------------------------------------------------------------------------
    Slideshow
-------------------------------------------------------------------------------*/
$.fn.CLIENTS_slickSlider = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.slick({
      infinite: true,
      speed: 300,
      nextArrow: '<a class="arrow-client next-arrow-client"></a>',
      prevArrow: '<a class="arrow-client prev-arrow-client"></a>',
      slidesToShow: 1,
      slidesToScroll: 1,
    });
  }
};


/*-------------------------------------------------------------------------------
    Slideshow
-------------------------------------------------------------------------------*/
$.fn.NEWS_slickSlider = function()
{
  var $target = $(this);

  if($target.length)
  {
    var initSlides = {
      autoplay: true,
      autoplaySpeed: 4000,
      speed: 300,
      nextArrow: '<a class="arrow-blog next-arrow-blog"></a>',
      prevArrow: '<a class="arrow-blog prev-arrow-blog"></a>',
      rows:0,
      slide: ".slide-item",
      arrows: true,
      dots: false,
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      pauseOnDotsHover: true,
      responsive: [
      {
        breakpoint: 1024,
        settings: {
          autoplay: true,
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: false,
          dots: false,
          arrows: true,
        }
      },
      {
        breakpoint: 767,
        settings: {
          autoplay: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          dots: false,
          arrows: true,
        }
      }]
    };

    $target.not(".slick-initialized").slick(initSlides);

    //Init
    $(window).on("load resize", function ()
    {
      if ($(window).width() < 994)
      {
        $target.not(".slick-initialized").slick(initSlides);
      }
      else if ($target.hasClass("slick-initialized"))
      {
        //$target.slick("unslick");
      }
    });
  }
};


/*-------------------------------------------------------------------------------
    Slideshow
-------------------------------------------------------------------------------*/
$.fn.PARTNERS_slickSlider = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.slick({
      infinite: true,
      speed: 300,
      arrows: false,
      slidesToShow: 6,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
          }
        }
      ]
    });
  }
};


/*-------------------------------------------------------------------------------
    Slideshow
-------------------------------------------------------------------------------*/
$.fn.SERVICES_slickSlider = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $filter_item = $('.filters-services__item');

    $target.slick({
        infinite: true,
        vertical:true,
        verticalSwiping:true,
        centerMode: true,
        autoplay:true,
        autoplaySpeed: 1500,
        centerPadding: '60px',
        nextArrow: '<span class="services-arrow services-arrow-next">></span>',
        prevArrow: '<span class="services-arrow services-arrow-prev"><</span>',
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 4,
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
            }
          }
        ]
      });

    $filter_item.click(function(e)
    {
      e.preventDefault();

      $filter_item.removeClass('active');

      $(this).addClass('active');

			$target.slick('slickUnfilter').slick('slickFilter','.'+$(this).data('filter'));

      return false;
		});

    $target.on('click', '.services-slider-item', function (e)
   	{
   		 var id = $(this).data("id");

   		 if(id == 3)
       {
   		 	window.location.href = '/projects-software/';
   		 }else if(id == 4){
   			 window.location.href = '/consulting_categories/bpr/';
   		 }else if(id == 5){
   			 window.location.href = '/web_categories/websites-mobiles/';
   		 }
    });
  }
};


/*-------------------------------------------------------------------------------
    What section
-------------------------------------------------------------------------------*/
$.fn.whatSection = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $html_body = $('html, body');

    $('.what-section__item--it .arrow-section').click(function (e) {
      var arrow = $(this);
      var parentItem = arrow.parents(".what-section__item");
      var hiddenInfo = arrow.parents(".what-section__item").find('.hidden-more-info');
      var left = Math.round(parentItem.position().left);

      anime({
       targets: ".what-section__item--it",
       translateX: "-"+left,
       easing: 'easeInOutSine',
        duration: 500,
        begin: function(anim) {
          $html_body.css('pointer-events','none');
        },
        complete: function(anim) {
          $html_body.css('pointer-events','all');
        }
      });
      e.stopPropagation();
    });

   $('.what-section__item--eshop .arrow-section').click(function (e) {
     var arrow = $(this);
     var parentItem = arrow.parents(".what-section__item");
     var hiddenInfo = arrow.parents(".what-section__item").find('.hidden-more-info');
     var left = Math.round(parentItem.position().left);

     anime({
       targets: ".what-section__item--eshop",
       translateX: "-"+left,
       easing: 'easeInOutSine',
       duration: 600,
       begin: function(anim) {
         $html_body.css('pointer-events','none');
       },
       complete: function(anim) {
         $html_body.css('pointer-events','all');
       }
     });
     e.stopPropagation();
   });

    $('.arrow-section').click(function (e) {
     var arrow = $(this);
     var parentItem = arrow.parents(".what-section__item");
     var hiddenInfo = arrow.parents(".what-section__item").find('.hidden-more-info');
     var left = Math.round(parentItem.position().left);

      parentItem.toggleClass('zindex');
      hiddenInfo.delay(50).fadeToggle(200);
      arrow.toggleClass('rotate');
      e.stopPropagation();
    });


   $('.close-icon').click(function (e) {
     var close = $(this);
     var parentItem = close.parents(".what-section__item");
     var className = $(parentItem).attr('class').split(" ");
     var classToScrollIT;

     if(className[1] === 'what-section__item--soft')
     {
       classToScrollIT = 'what-section__item--it';
     }else{
       classToScrollIT = className[1];
     }

     var hiddenInfo = close.parents(".what-section__item").find('.hidden-more-info');
     var left = Math.round(parentItem.position().left);
     var arrow = close.parents(".what-section__item").find('.arrow-section');

     anime({
       targets: '.'+classToScrollIT,
       translateX: "-"+left,
       easing: 'easeInOutSine',
       duration: 500,
       begin: function(anim) {
         $('html, body').css('pointer-events','none');
       },
       complete: function(anim) {
         $('html, body').css('pointer-events','all');
       }
     });
     parentItem.toggleClass('zindex');
     hiddenInfo.fadeToggle(200);
     arrow.toggleClass('rotate');
     e.stopPropagation();
   });

   $('body').click(function (e)
   {
     if($('.hidden-more-info').is(':visible'))
     {
       var visible = $('.hidden-more-info:visible').parents(".what-section__item");
       var className = $(visible).attr('class').split(" ");
       var classToScroll;

       if(className[1] === 'what-section__item--soft'){
         classToScroll = 'what-section__item--it';
       }else{
         classToScroll = className[1];
       }

       var hiddenInfo = visible.find('.hidden-more-info');
       var left = Math.round(visible.position().left);
       var arrow = visible.find('.arrow-section');

       anime({
         targets: '.'+classToScroll,
         translateX: "-"+left,
         easing: 'easeInOutSine',
         duration: 500,
         begin: function(anim) {
           $('html, body').css('pointer-events','none');
         },
         complete: function(anim) {
           $('html, body').css('pointer-events','all');
         }
       });
       visible.toggleClass('zindex');
       hiddenInfo.fadeToggle(200);
       arrow.toggleClass('rotate');
       e.stopPropagation();
     }
   });
  }
};





/*-------------------------------------------------------------------------------
    Visible Element
-------------------------------------------------------------------------------*/
$.fn.visibleElementFN = function()
{
  var $target = $(this);

  if($target.length)
  {
    $(window).scroll(function()
    {
      $target.each(function(i, el)
      {
        var _el = $(el);

        if (_el.onViewportFN(true))
        {
          _el.addClass("element-visible");
        }
      });
    });
  }
};


/*-------------------------------------------------------------------------------
    On Viewport Element - Visible
-------------------------------------------------------------------------------*/
$.fn.onViewportFN = function(partial)
{
  var $t            = $(this),
      $w            = $(window),
      viewTop       = $w.scrollTop(),
      viewBottom    = viewTop + $w.height(),
      _top          = $t.offset().top,
      _bottom       = _top + $t.height(),
      compareTop    = partial === true ? _bottom : _top,
      compareBottom = partial === true ? _top : _bottom;

return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
};




/******************************************************************************************
***************** FILTERING *************************************************************
*******************************************************************************************/
/*----------------------------------------------------------------------------------
                    Callback: Render HTML - AJAX Calls
-----------------------------------------------------------------------------------*/
var renderHtml = function(html)
{
 var result = String(html).replace(/<\!DOCTYPE[^>]*>/i, '')
              .replace(/<(html|head|body|title|script)([\s\>])/gi,'<div id="get-html-$1"$2')
              .replace(/<\/(html|head|body|title|script)\>/gi,'</div>');

 return result;
};


/*----------------------------------------------------------------------------------
                    FN: filtering - ajax call
-----------------------------------------------------------------------------------*/
$.fn.ajax_filtered_products = function(search_data,window_state)
{

  //Get values and target Elements
  var $wrapper = $(".list-customers-section"),
      _path = window.location.pathname,
      _url = (window_state==="false")? _path+'?'+search_data : search_data;

  //Ajax request
  $.ajax({
      url : _url,
      beforeSend: function()
      {
        $("html, body").animate({scrollTop: $('.wrap-filters-menus').offset().top-70}, 600);

        //Active class
        $wrapper.toggleClass("loading");
      },
      error: function( jqXHR, textStatus, errorThrown )
      {
        //Error
        alert( 'The following error occured: ' + textStatus +' Try to refresh the page or contact us. ' + errorThrown);

        $wrapper.toggleClass("loading");
      },
      success : function( data_return, textStatus, jqXHR )
      {
          var $data	= $(renderHtml(data_return)),
              $resultsHTML = $data.find(".list-customers-section").html();

          //Wrapper opacity
          setTimeout(function()
          {
            $wrapper.toggleClass("loading");
          },1200);

          //Append results
          $wrapper.html($resultsHTML);

          $(window).activeFilters();
      }
  });
};



/*----------------------------------------------------------------------------------
                  FN: Window change
-----------------------------------------------------------------------------------*/
$.fn.filters_window_change = function()
{
  if($(this).length)
  {
    var $main_wrapper = $(".list-customers-section");

    //Window state event
    $(window).bind('statechange',function()
    {
      var History = window.History,
          State = History.getState(),
          _title = document.title || null,
          search_data	= State.url,
          window_state = true;

      /* FN: AJAX FILTERING */
      $(window).ajax_filtered_products(search_data,window_state);
    });
  }
};

/*----------------------------------------------------------------------------------
        ****** FN: Window location.search api for searching params ******
-----------------------------------------------------------------------------------*/
$.fn.search_customers_init = function(search_data)
{
  if(search_data !== "")
  {
    var History = window.History;
    var window_state = false;

    //Push state - filters_window_change
    History.pushState(null,null,window.location.pathname+'?'+search_data);
  }
  else
  {
    //Reset - go to default
    window.location = $(".wrap-filters-menus").attr("data-href");
  }
};

/*----------------------------------------------------------------------------------
                  ******  FN: Search filters init ******
-----------------------------------------------------------------------------------*/
$.fn.searchFiltersEvents = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $menu_filter_item = $target.find('.filter-item'),
        $window = $(window),
        $filter_service = $target.find('.radio-btn'),
        $filter_industry = $target.find('.checkbox-btn'),
        _mergeAllParams = {};

    //Get active select filters
    if (window.location.href.indexOf("?") !== -1) { $window.activeFilters(); }

    //SERVICES
    $menu_filter_item.click(function(e)
    {
      e.preventDefault();

      //Defaults
      var SERVICES_params = {};

      //Menus
      var $menu_industries = $(this).parents('.list-customers-industries').eq(0);
      var $menu_services = $(this).parents('.list-customers-services').eq(0);

      if($menu_industries.length)
      {
        $('.list-customers-industries .filter-item').removeClass('active');
        $(this).toggleClass('active');
      }

      /* SERVICES */
      if($menu_services.length)
      {
        //Remove class for services - because its radio button
        $('.list-customers-services .filter-item').removeClass('active');
        $(this).addClass('active');
      }

      $(".active:not(.all)").each(function()
      {
        var _s_val = $(this).attr("data-id");

        if(_s_val!=="all")
        {
          var _s_param_name = $(this).attr("data-param"),
              _s_param_val = $(this).attr("data-id");

          //Build object
          SERVICES_params[_s_param_name] = _s_param_val;
        }
      });

      /* Merge all
      -------------------------------------------------------*/
      _mergeAllParams = $.extend({},SERVICES_params);

      /* Search request */
      $window.search_customers_init($.param(_mergeAllParams));

      //console.log("Params: "+$.param(_mergeAllParams));

    }); //END $menu_filter_item CLICK EVENT
  }
};


/*----------------------------------------------------------------------------------
            ****** FN: show active select filters ******
-----------------------------------------------------------------------------------*/
$.fn.activeFilters = function()
{
  //defaults
  var _this = $(this);
  var tag_filters = $(".wrap-filters-menus");

  if($(".wrap-filters-menus").find('active').length)
  {
    //Parse query
    query.parse({ query: window.location.search.substring(1) });

    //Get params
    var _industries     = query.get('industries'),
        _service     = query.get('type');

    //Uncomment for dubugging
    //console.log("_industries:"+_industries+" | _service:"+_service);

    //TAG: Brand select
    if(_industries !== "undefined")
    {
      var _industriesArr = _industries.split(',');

      for (i = 0; i < _industriesArr.length; i++)
      {
        if(tag_filters.find('.filter-item[data-id="'+_industriesArr[i]+'"]').length)
        {
          tag_filters.find('.filter-item[data-id="'+_industriesArr[i]+'"]').addClass('active');
        }
      }
    }

    //TAG: Color select
    if(_service !== "undefined")
    {
      //Remove class for services - because its radio button
      $('.list-customers-services .filter-item').removeClass('active');

      var _serviceArr = _service.split(',');
      var _targetService = tag_filters.find('.filter-item[data-id="'+_service+'"]');

      for (i = 0; i < _serviceArr.length; i++)
      {
        if(tag_filters.find('.filter-item[data-id="'+_serviceArr[i]+'"]').length)
        {
          tag_filters.find('.filter-item[data-id="'+_serviceArr[i]+'"]').addClass('active');
        }
      }
    }
  }
};





/*-----------------------------------------------------------------------------------------------
    1. NAV: build coordinates array for each page - IMPORTANT: trigger when content is fully load
  ------------------------------------------------------------------------------------------------*/
  $.fn.Timeline_scrollSections = function()
  {
     var $target = $(this);

     if($target.length)
     {
       var $main_nav = $("#timeline-nav");
       var item = 0;
       var items = [];

       //Build offset coordinates and prepare to calculate
       $target.each(function()
       {
          var pos = $(this).offset();

          item = Math.round(pos.top);

          //Array
          items.push(item);

          $(this).attr("data-coordinates",item);

       })
       .promise().done(function()
       {
         var i = 0;

         //put coordinates on menu
         for (i=0; i<=$target.length; i++)
         {
           $main_nav.find("li:eq("+i+")").attr("data-coordinates",items[i]);
         }
       });
     }
  };

  /*-----------------------------------------------------------------------------------------------
    2. Event for main nav
  ------------------------------------------------------------------------------------------------*/
  $.fn.Timeline_NavScrollingEvent = function()
  {
    var $target = $(this);

    if($target.length)
    {
      if($target.length)
      {
        var $main_nav_links = $target.find("a");

        //EVENT for scrolling
        $main_nav_links.click(function(e)
        {
          e.preventDefault();

          var _page = $(this).attr('data-section');
          var $target_page = $(".scroll-nav-section[data-section="+_page+"]");
          var $hashtag = window.location.hash;

          $target.find(".active").removeClass("active");

          $(this).parents("li").eq(0).addClass("active");

          if($target_page.length)
          {
            var $page_offset = $target_page.offset().top;

            $("body,html").animate({
              scrollTop: $page_offset-80
            },800);

            //Remove hashtag on click
            if($hashtag.length)
            {
              window.history.pushState(null, null, ' ');
            }
          }
          else
          {
            alert("This page does not exist. Please contact us.");
          }

          return false;
        });
      }
    }
  };

  /*-----------------------------------------------------------------------------------------------
      2. NAV: callback after build coordinates - active class
  -----------------------------------------------------------------------------------------------*/
  $.fn.Timeline_activeClass = function()
  {
    var $target = $(this);

    if($target.length)
    {
      $(window).scroll(function()
      {
          var $sections = $(".scroll-nav-section");
          var scroll_active = $(window).scrollTop();
          var _target = '';

          // change sub nav active
          $sections.each(function()
          {
            var _gutterSpace = 280;
            var _s = scroll_active + _gutterSpace;
            var _top = $(this).attr('data-coordinates');
            var _bottom = _top + $(this).height();

            if( _s > _top && _s < _bottom)
            {
              _target = '.item-' + $(this).attr('data-section'); //id
            }
          });

          //console.log(scroll_active);
          $target.find("li").removeClass('active');

          if(_target.length)
          {
            $(_target).addClass('active');
          }
      });
    }
  };


  /*-----------------------------------------------------------------------------------------------
     menu scroll hashtag on load
  ------------------------------------------------------------------------------------------------*/
  $.fn.Timeline_hashtagScroll = function()
  {
    //Onload - Search for hashtag
    var hashtag = window.location.hash;

    if(hashtag.length)
    {
      var $get_section = window.location.hash.substr(1);
      var $target_section = $("#"+$get_section);

      if($target_section.length)
      {
        var $page_offset = $target_section.offset().top;

        $("body,html").animate({
          scrollTop: $page_offset-80
        },1200);
      }
      else
      {
        alert("This page does not exist. Please contact us.");
      }
    }

  };


  /*-------------------------------------------------------------------------------
      Show Timeline nav
  -------------------------------------------------------------------------------*/
  $.fn.Timeline_headerScrollingFN = function()
  {
    var $target = $(this);

    if($target.length)
    {
      var _heightTop = $('.timeline-wrap').offset().top+20,
          _heightBottom = $('.timeline-wrap').offset().top + $('.timeline-wrap').height(),
          $body = $('body'),
          $window = $(window),
          $pos = "";

      $window.scroll(function()
      {
         $pos = $window.scrollTop();

         if($pos>=_heightTop)
         {
           $body.addClass("show-timeline-nav");
         } else {
           $body.removeClass("show-timeline-nav");
         }

         if($pos>=_heightBottom)
         {
           $body.removeClass("show-timeline-nav");
         }
      });
    }
  };



})(jQuery); /******** Fully reference jQuery after this point. *************/
