<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<section class="vertical-scrolling page-header" style="background-image: url('<?= get_the_post_thumbnail_url(); ?>')">

    <div id="what-page-header" class="p-0 d-flex align-items-start justify-content-center container page-header__inner">
        <div class="page-title">
            <h1 class="white-font"><?php the_title(); ?></h1>
        </div>
        <div class="page-content">

            <?php the_content(); ?>

        </div>
    </div>
</section>
