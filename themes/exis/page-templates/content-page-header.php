<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
?>

<section class="page-header-default page-header-default-template">

    <div class="container">
        <div class="page-title-header">
            <h1 class="white-font blog-page-title"><?php the_title(); ?></h1>
            <?php if( get_field('page_subtitle') ): ?>
                <p><?php the_field('page_subtitle'); ?></p>
            <?php endif; ?>
        </div>
    </div>
</section>
