<section id="parallax-container" class="vertical-scrolling fp-auto-height front-parallax-section">
	<div class="container d-flex align-items-center front-parallax-section__inner">
		<div class="ex-moto">
		<?= __('<strong>Software</strong> and <strong>technology</strong> solutions that enable <strong>companies to thrive</strong>
in the <strong>digital era.</strong>', 'exis'); ?>
		</div>
		<div class="cursor-circle"><span>See More</span></div>
	</div>
</section>
