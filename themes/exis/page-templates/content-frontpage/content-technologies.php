<section class="section-technologies-area-wrap vertical-scrolling fp-auto-height websites-technologies--front">
    <div class="container">
        <h5><?= __('Technologies', 'exis'); ?></h5>
        <div class="technologies-container">
            <?php if( get_field('technologies') ): ?>
                <?php the_field('technologies'); ?>
            <?php endif; ?>
        </div>
    </div>
</section>
