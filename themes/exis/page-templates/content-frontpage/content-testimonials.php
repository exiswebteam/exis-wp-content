<?php
$args = array(
    'post_type' => 'testimonials',
    'post_status' => 'publish',
);

$query_testimonials = new WP_Query( $args );

if( $query_testimonials->have_posts() ) :
?>

<div class="slider-client">
<?php
  while ( $query_testimonials->have_posts() ) : $query_testimonials->the_post();

    $content = get_the_content();
    $content = wp_strip_all_tags(apply_filters('the_content', $content));
    $client = (!empty(get_field('client_name')))? get_field('client_name'): '';
    $role = (!empty(get_field('client_role')))? get_field('client_role'): '';
  ?>
  <div class="d-flex align-items-center slider-current-me__info">
      <div class="client-testimonial">
          <p class="client-testimonial_p"><?= $content; ?></p>
      </div>

      <div class="d-flex align-items-center clients-info--front">
          <img class="client_logo" src="<?= get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
          <div class="client-testimonial--info">
              <p class="client_name"><?= $client; ?></p>
              <p class="client_role"><?= $role; ?></p>
          </div>
      </div>
  </div>
  <?php
    endwhile;
    wp_reset_query();
  endif;
  ?>
</div>
