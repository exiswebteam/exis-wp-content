<section class="section-partners-area-wrap vertical-scrolling front-partners-section">
	<div class="inner-wrapper">
		<div class="container front-partners-section__in">
	        <div class="front-partners__texts">
	            <p class="front-partners__desc">
	                <?= __(' Experienced engineers with proven know how on digital transformation.', 'exis');?>
	            </p>
	        </div>
		</div>
    <div class="partners-grid">
        <div class="container d-flex flex-wrap">
					<?php
					if( have_rows('partners_gallery') ):
						while ( have_rows('partners_gallery') ) : the_row();
						$image = get_sub_field('image');
						$desc = get_sub_field('description');
						$img_srcset = wp_get_attachment_image_srcset( $image['id'], 'full' );
						?>
						<div class="text-center partners-item">
              <img src="<?php echo $image['url']; ?>" alt="<?= $image['title'].' '.$desc; ?>">
              <p><?= $desc; ?></p>
            </div>
						<?php
					endwhile;
				endif;
				?>
        </div>
    </div>
	</div>
</section>
