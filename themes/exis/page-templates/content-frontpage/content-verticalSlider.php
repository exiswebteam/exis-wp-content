<?php if(!wp_is_mobile()): ?>
<section class="section-vertical-slider-area-wrap vertical-scrolling services-slider__container">
       <div class="services-slider-section">
           <div class="services-slider-section__inner">
               	 <div class="offset-md-2 col-md-3 services-left-side">
                   	<div class="front-services-text-filter animate">
                		<h3 class="light-big-font white-font">
                			<?= __('Technology<br/>Solutions','exis'); ?>
                		</h3>
                		<div class="d-flex justify-content-between filters-services">
                			<div id="software-filter" data-filter="software" class="filters-services__item"><?= __('Software','exis'); ?></div>
        					<div id="consult-filter" data-filter="consulting" class="filters-services__item"><?= __('Consulting','exis'); ?></div>
							<div id="web-filter" data-filter="web-mobile" class="filters-services__item"><?= __('Web / Mobile','exis'); ?></div>
                		</div>
            		</div>
               	 </div>
               	 <div class="col-md-5 p-0 services-right-side">
               	 	<div class="services-slider">
               	 	<?php
               	 	$args = array(
               	 	    'post_type' => 'services',
               	 	    'post_status' => 'publish',
               	 	    'posts_per_page' => -1,
               	 	    'orderby' => 'rand',
               	 	);
               	 	//$posts = new WP_Query( $args );
               	 	$loop = new WP_Query( $args );
               	 	while ( $loop->have_posts() ) : $loop->the_post();
               	 	$category = get_the_terms($post->ID, 'services');
               	 	?>
               	 		<div class="services-slider-item <?= $category[0]->slug; ?>" data-id="<?= $category[0]->term_id; ?>">
               	 			<h4><?= get_the_title(); ?></h4><span class="category-item"><?= $category[0]->name; ?></span>
               	 		</div>
               	 		     <?php
                        endwhile;
                    wp_reset_query();
                    ?>
               	 	</div>
               	 </div>
           </div>
       </div>
</section>
<?php endif; ?>
