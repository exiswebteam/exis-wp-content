<div class="section-services-area-wrap vertical-scrolling front-services-section">
	<div class="container d-flex flex-wrap align-items-center front-services-section__inner">

		<div class="front-services-wrapper-text animate">
  		<h3 class="title" >Leverage our <?= date("Y")-2001; ?> years long experience in technology and advanced software solutions</h3>
		</div>
		<div class="hero-section">
          <div class="card-flex">
            <a class="card-item" href="<?= get_the_permalink(123) ?>">
               <div class="card__background">
                 <div class="card__content">
                <h3 class="card__heading"><?php
                    $Numbers = get_field('custom_software', 'option');
                    echo __("<span>{$Numbers}</span> <br> custom <br> business <br> software <br> solutions", 'exis')?></h3>
              </div>
               </div>
            </a>
            <a class="card-item" href="<?= get_category_link( 17 ) ?>">
               <div class="card__background">
                <div class="card__content">
  					<h3 class="card__heading"><?php
                        $Numbers = get_field('consulting', 'option');
                        echo __("<span>{$Numbers}</span> <br> technology <br>consulting <br> projects", 'exis')?></h3>
              </div>
               </div>
            </a>
            <a class="card-item" href="<?= get_category_link( 16 ) ?>">
               <div class="card__background">
                 <div class="card__content">
 					 <h3 class="card__heading"><?php
                         $Numbers = get_field('websites', 'option');
                         echo __("<span>{$Numbers}</span> <br> websites", 'exis')?></h3>
              </div>
               </div>
            </a>
            <a class="card-item" href="<?= get_category_link( 15 ) ?>">
              <div class="card__background">
               <div class="card__content">
					<h3 class="card__heading"><?php
                        $Numbers = get_field('e-shops', 'option');
                       echo __("<span>{$Numbers}</span> <br> e-shops", 'exis')?></h3>
              </div>
              </div>
            </a>
          </div>
       </div><!-- END .hero-section -->
	</div>
	    <?php if(wp_is_mobile()){ ?>
	    <p class="scroll-text scroll-text--white">
	    	<span class="arrow-scroll__img left-arrow"></span>
	    	swipe
	    	<span class="arrow-scroll__img right-arrow"></span>
	    </p>
	    <?php }?>
</div><!-- END .section-services-area-wrap -->
