<section class="section-news-cta-area-wrap vertical-scrolling fp-auto-height front-news-section">
	<div class="container mx-auto align-items-center">
		<div class="news-section">
			<h4 class="light-small-font title-item"><?= __('Stay tuned', 'exis');?></h4>
			<div class="row news-slider">
				<?php
				$args = array(
						'posts_per_page' => 6,
						'post_type' => array('post'),
						'orderby' => 'date',
						'order'   => 'DESC',
				);
				$latest = new WP_Query( $args );

				while($latest->have_posts()) : $latest->the_post();
				?>
				<div class="col-sm-12 col-md-4 slide-item">
					<div class="news-item">
						<a href="<?= get_the_permalink(); ?>">
							<?= wp_get_attachment_image( get_post_thumbnail_id(), 'large' ); ?>
						</a>
						<h4><?= get_the_title();?></h4>
						<a href="<?= get_the_permalink(); ?>"><?= __('read more', 'exis')?></a>
					</div>
				</div>
				<?php
				endwhile;
				?>
			</div>
		</div>
		<div class="cta-lets-discuss-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<section class="cta-discuss-section">
						<h3 class="white-font">
						<?= __('We believe that the <span>power of technology </span>
			                offers <span>countless opportunities </span>
			                to maximize <span>business performance.</span>', 'exis');?>
						</h3>
						<a class="white-font" href="<?php echo get_permalink(368);?>">
			                <?= __("<span>Let's discuss</span><br>
			                about your business", 'exis')?>
						</a>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
