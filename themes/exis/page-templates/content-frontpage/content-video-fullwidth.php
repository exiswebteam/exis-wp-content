<section class="vertical-scrolling front-video-section">
    <div class="container d-flex justify-content-between align-items-center front-video-section_inner">

        <div class="front-video-wrapper-text">
            <h1 class="animated fadeIn delay-1s">
                <?= __('666We deliver <strong>technology</strong> that gets your business where you have <strong>imagined</strong>.','exis'); ?>
            </h1>
            <div class="d-flex align-items-top">
                <div class="services-item animated fadeIn delay-2s">
    				<a href="<?= get_permalink(711); ?>">
    					<?= __('Software Development', 'exis'); ?>
    				</a>
                </div>
                <div class="services-item animated fadeIn delay-3s">
    				<a href="<?= get_permalink(849); ?>">
    					<?= __('Technology Consulting', 'exis'); ?>
    				</a>
                </div>
                <div class="services-item animated fadeIn delay-4s">
    				<a href="<?= get_permalink(93); ?>">
    					<?= __('Web & Mobile Applications', 'exis'); ?>
    				</a>
                </div>
            </div>
        </div>

        <div class="front-video-wrapper-full">
            <video autoplay loop muted>
                <source src="https://ak9.picdn.net/shutterstock/videos/1013880989/preview/stock-footage-abstract-blue-and-violet-geometrical-plexus-flowing-particles-movement-on-black-background-with.mp4">
            </video>
        </div>

    </div>
</section>
