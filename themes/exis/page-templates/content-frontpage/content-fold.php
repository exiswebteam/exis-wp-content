<section class="section-hero-area-wrap vertical-scrolling front-fold-section">
    <div class="container d-flex justify-content-between align-items-center front-fold-section_inner">

        <div class="front-fold-wrapper-text">
            <h1 class="animated fadeIn delay-1s main-title ">
                <?= __('We deliver technology that gets your business where you have imagined.','exis'); ?>
            </h1>
        </div>

        <div class="front-fold__services">
            <div class="d-flex services_fold__container">
                <div class="services-item animated fadeIn delay-2">
          				<a href="<?= get_permalink(711); ?>">
          					<?= __('Business<br>Software', 'exis'); ?>
          				</a>
                </div>

                <div class="services-item animated fadeIn delay-3">
          				<a href="<?= get_permalink(849); ?>">
          					<?= __('Technology<br>Consulting', 'exis'); ?>
          				</a>
                </div>

                <div class="services-item animated fadeIn delay-4">
          				<a href="<?= get_permalink(483); ?>">
          					<?= __('Websites<br>& E-Shops', 'exis'); ?>
          				</a>
                </div>
            </div>

        </div>
        <?php
        if(!wp_is_mobile()){ ?>
        <div class="front-video-wrapper-full">
            <video autoplay loop muted>
            <source src="<?php the_field('hero_area_video'); ?>">
            </video>
        </div>
        <?php } ?>
    </div>
</section>
