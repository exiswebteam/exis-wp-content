<section class="section-clients-area-wrap vertical-scrolling <?php if(is_front_page()){ ?>front-clients-section<?php } ?>">
	<div class="container d-flex flex-wrap align-items-center front-clients-section__inner">
        <div class="front-clients-wrapper-text">
            <h2 class="light-big-font">
                <?= __('Our clients<br/> do the talking for us','exis'); ?>
            </h2>
        </div>

		<?php get_template_part('page-templates/content-frontpage/content-testimonials'); ?>

		<?php if(is_front_page()): ?>
        <section class="container px-0 front-logos-section">
            <h3 class="text-center"><?= __('Trusted by', 'exis'); ?></h3>
            <div class="front-logos">
                <?php
                if( have_rows('logos_home', 13) ):
                    while ( have_rows('logos_home', 13) ) : the_row();
										?>
                        <div class="websites-logo__item">
                            <img src="<?php the_sub_field('logos', 13); ?>" alt="exis <?php the_title(); ?>" width="160">
                        </div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>
        </section>
			<?php endif; ?>
	</div>
</section>
