<?php
$bg_image = get_field('ecosystem_background_image');
$exis_logo = get_field('ecosystem_exis_logo');

?>

<section class="who-techn-partners-section ecosystem-main-wrapper">
    <h2 class="text-center mb-sm-0 who-techn-partners__title"><?= __(get_field('ecosystem_title'), 'exis'); ?></h2>
    <div id="ecosystem-paralax" class="tech-wheel-section ecosystem-inner-wrap visible-el" style="background-image: url(<?= $bg_image['url']; ?>);">
      <div class="logo-wrap parallax-init" data-property="top" style="background-color: <?= get_field('ecosystem_circle_color');?>;">
        <img src="<?= $exis_logo['url']; ?>" alt="<?php the_title(); ?>">
      </div>
        <?php
        if( have_rows('ecosystem_technologies_repeater') ):
          $i = 1;
          while ( have_rows('ecosystem_technologies_repeater') ) : the_row();
          ?>
          <h4 class="title title-<?= $i++; ?>"><?= get_sub_field('title'); ?></h4>
          <?php
          endwhile;
        endif;
        ?>
    </div>
</section>
