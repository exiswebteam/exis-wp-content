<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
?>

<section class="vertical-scrolling page-header who-header">

    <div id="who-page-header" class="p-sm-0 d-flex align-items-center justify-content-center container black-font page-header__inner">
        <div class="page-title who-title">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="page-content black-font who-content">

            <?php the_content(); ?>

        </div>
    </div>
</section>
