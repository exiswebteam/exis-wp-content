<div class="mobile-nav-wrap">

  <div class="inner-wrap">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <?php
  				the_custom_logo();
  				?>

          <a class="close-item active" href="#">
            <span class="line line-1"></span>
            <span class="line line-2"></span>
            <span class="line line-3"></span>
			<span style="display: none;">Mobile menu</span>
          </a>
        </div>

        <div class="col-md-4">
          <?php wp_nav_menu(
            array(
              'theme_location'  => 'mobile_nav',
              'container_class' => 'mobile-menu',
              'container_id'    => 'main-nav-1',
              'menu_class'      => 'exis-nav-mobile menu-1',
              'fallback_cb'     => '',
              'menu_id'         => 'exis-menu-1',
              'depth'           => 2
            )
          );  ?>
        </div>

        <div class="col-md-4">
          <?php wp_nav_menu(
            array(
              'theme_location'  => 'mobile_nav_2',
              'container_class' => 'mobile-menu ',
              'container_id'    => 'main-nav-2',
              'menu_class'      => 'exis-nav-mobile menu-2',
              'fallback_cb'     => '',
              'menu_id'         => 'exis-menu-2',
              'depth'           => 2
            )
          );  ?>
        </div>

        <div class="col-md-4">
          <?php wp_nav_menu(
            array(
              'theme_location'  => 'mobile_nav_3',
              'container_class' => 'mobile-menu',
              'container_id'    => 'main-nav-3',
              'menu_class'      => 'exis-nav-mobile menu-3',
              'fallback_cb'     => '',
              'menu_id'         => 'exis-menu-3',
              'depth'           => 2
            )
          );  ?>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="exis-info">Click to view indicative customer success stories</div>
		</div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="front-logos">
            <?php
            if( have_rows('clients_area_mobile', 'option') ):
                while ( have_rows('clients_area_mobile', 'option') ) : the_row();
                $image = get_sub_field('image');
                $link = get_sub_field('link');
                $target = strtolower(get_sub_field('open_new_window'));
                ?>
                    <div class="websites-logo__item">
                      <?php if(!empty($link)): ?>
                      <a href="<?= $link; ?>" class="logo-hover" data-hover="View project" target="<?= ($target=='yes')? '_blank': '_self'; ?>">
                      <?php endif; ?>
                        <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" width="160">
                      <?php if(!empty($link)): ?>
                      </a>
                      <?php endif; ?>
                    </div>
                <?php
                endwhile;
            endif;
            ?>
        </div>
      </div>
    </div>
  </div>

  <?php
  //Get fields
  $years = get_field('exis_logo_years','option');
  $contact = get_field('footer_nav_lets_discuss','option');
  $copyrights = get_field('footer_nav_copyrights','option');
  ?>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <?php if(!empty($years)): ?>
        <div class="years-experience" style="background-image: url(<?= $years['url']; ?>);"></div>
        <?php endif; ?>

        <?php if(!empty($contact)): ?>
        <div class="contact-exis">
          <?php echo $contact; ?>
        </div>
        <?php endif; ?>

        <?php if(!empty($copyrights)): ?>
        <div class="exis-info">
          <?php echo $copyrights; ?> EXIS &copy;<?= date("Y"); ?>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </div>

</div>
