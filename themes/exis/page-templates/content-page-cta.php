<section class="vertical-scrolling fp-auto-height page-cta-section page-cta-section--black">
    <div class="container d-flex align-items-center">
        <div class="col-md-12 d-flex align-items-end page-cta-discuss-section">
            <h3 class="white-font">
                <?= __('We believe that the <span>power of technology </span>
                offers <span>countless opportunities </span>
                to maximize <span>business performance.</span>', 'exis');?>
            </h3>
            <?php /* was 354 */
			if(!is_page(368)){ ?>
            <a class="white-font page-cta-link" href="<?php echo get_permalink(368);?>">
                <span><?php echo __('Let\'s discuss', 'exis');?></span>
            </a>
            <?php } ?>
        </div>
    </div>
</section>
