<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
?>

<section class="page-header-default">

    <div class="container">
        <div class="page-title-header">
            <h1 class="page-title white-font blog-page-title"><?= __('Projects', 'exis'); ?></h1>
            <p><?= __('We are proud of our deliverables and results.', 'exis'); ?></p>
        </div>
        <div class="projects-categories">
            <ul class="navbar-nav projects-categories__list">
                <li class="nav-item <?php echo is_page(123) ? "active" : ""?> projects-categories__list-item">
                    <a class="nav-link" href="<?= get_permalink(123); ?>">
                        <?= __('Custom Software', 'exis') ?>
                    </a>
                </li>
                <li class="nav-item <?php echo is_tax('consulting_categories') || has_term('', 'consulting_categories')  ? "active" : ""?>   projects-categories__list-item">
                    <a class="nav-link" href="<?= get_category_link( 17 ) ?>">
                        <?= __('TECHNOLOGY CONSULTING', 'exis') ?>
                    </a>
                </li>
                <li class="nav-item <?php echo is_tax('web_categories', 16) || has_term(16, 'web_categories')  ? "active" : ""?>  projects-categories__list-item">
                    <a class="nav-link" href="<?= get_category_link( 16 ) ?>">
                        <?= __('WEBSITES & MOBILE', 'exis') ?>
                    </a>
                </li>
                <li class="nav-item <?php echo is_tax('web_categories', 15) || has_term(15, 'web_categories') ? "active" : ""?> projects-categories__list-item">
                    <a class="nav-link" href="<?= get_category_link( 15 ) ?>">
                        <?= __('E-SHOPS', 'exis') ?>
                    </a>
                </li>
                <li class="nav-item <?php echo is_page(159) ? "active" : ""?> projects-categories__list-item">
                    <a class="nav-link" href="<?= get_permalink(159); ?>">
                        <?= __('LIST OF ALL CUSTOMERS', 'exis') ?>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</section>
