<a href="#" class="scroll-top">
	<?php $arrow = get_field('arrow_scroll_top','option'); ?>
	<div class="arrow-top"></div>
	<span class="screen-reader-text">Go to Top</span>
</a>
