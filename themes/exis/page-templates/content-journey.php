<?php
//Desc
$timeline_radio = get_field('timeline_activate');
$timeline_desc = get_field('timeline_desc');

$args = array(
  'post_type' => array('exis_journey'),
  'posts_per_page' => -1,
  'orderby' => 'date',
  'order'   => 'ASC',
  'post_status' => 'publish',
);

//if desc exist
if($timeline_radio=='1'):

//vars
$counter_nav = 1;
$counter_section = 1;

//Query
$timeline_query = new WP_Query( $args );

if ( $timeline_query->have_posts() ):

?>

<!-- Timeline wrap -->
<div class="timeline-wrap visible-el">

  <div class="timeline-desc">
    <?= $timeline_desc; ?>
  </div>

  <nav id="timeline-nav" class="timeline-nav">
    <ul>
      <?php
      while ( $timeline_query->have_posts() ): $timeline_query->the_post();
      $count_nav_items = $counter_nav++;
      $nav_year = get_the_date('Y');
      ?>
      <li class="menu-item item-<?= $count_nav_items; ?>" data-coordinates="">
        <a href="#year-<?= $nav_year; ?>" class="link" data-section="<?= $count_nav_items; ?>">
          <span><?= $nav_year; ?></span>
        </a>
      </li>
      <?php
      endwhile;

      wp_reset_postdata();
      ?>
    </ul>
  </nav>

  <div class="container">
   <div class="inner">

     <?php
     while ( $timeline_query->have_posts() ): $timeline_query->the_post();

     $section_year = get_the_date('Y');
     ?>
     <!-- Timeline Section -->
     <div class="section-year scroll-nav-section visible-el" data-section="<?= $counter_section++; ?>" data-year="<?= $section_year; ?>">
       <span class="icon"><?= wp_get_attachment_image( get_post_thumbnail_id(), 'full' ); ?></span>
       <div class="item-wrap">
         <article class="item img"></article>
         <artlce class="item text">
           <hgroup>
             <h2><?= $section_year; ?></h2>
             <h3><?= get_the_title(); ?></h3>
           </hgroup>
           <?php the_content(); ?>
         </artlce>
       </div>
     </div>
     <?php
     endwhile;

     wp_reset_postdata();
     ?>

   </div><!-- .inner -->
 </div><!-- .container -->
</div><!-- .timeline-wrap -->

<?php endif; // if have posts ?>

<?php endif; ?>
