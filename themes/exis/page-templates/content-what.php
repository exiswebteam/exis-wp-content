<section class="section-what-area-wrap vertical-scrolling  what-section <?php echo is_front_page() ? 'front-what-section' : '' ?>">
    <?php
    if(is_front_page()):
        ?>
        <div class="what-section__main-title">
            <h3 class="light-big-font white-font">
                <?php echo __('What we do', 'exis'); ?>
            </h3>
            <p class="white-font"><?= __('Always by your side to provide cost-effective technology solutions.', 'exis'); ?></p>
        </div>
    <?php
    endif;
    ?>
    <div class="what-section__services animated fadeInUp">
        <div class="what-section__item what-section__item--soft">
            <div class="what-section__item--cont">
                <h2><?= __('Business</br>Software','exis') ?></h2>
                 <p class="mb-2"><?= __('We design and develop premium custom software, assisting:','exis') ?></p>
                 <ul class="mb-4">
                    <li>
                        <?= __('Multinationals','exis') ?>
                    </li>
                    <li>
                        <?= __('Enterprises','exis') ?>
                    </li>
                    <li>
                        <?= __('SMEs','exis') ?>
                    </li>
                    <li>
                        <?= __('Startups','exis') ?>
                    </li>
                </ul>
                <p>
                    <?= __('to become more effective, creative and efficient, and allowing them to focus on their core business.','exis') ?>
                </p>
                <div class="arrow-section text-right">
                  <span class="arrow-icon purple"></span>
                </div>
            </div>
            <div class="hidden-more-info">
                <div class="hidden-more-info__inner">
                    <h2>
                        <?= __('Business flexibility requires custom software'); ?>
                    </h2>
                    <p class="mb-2"><?= __('In the very usual occasions that off-the-shelf software covering a particular business need:','exis') ?></p>
                    <ul class="mb-4">
                        <li>
                            <?= __('does not exist,','exis') ?>
                        </li>
                        <li>
                            <?= __('is too expensive,','exis') ?>
                        </li>
                        <li>
                            <?= __('does not meet 100% of the business requirements,','exis') ?>
                        </li>
                        <li>
                            <?= __('has proven daunting to use','exis') ?>
                        </li>
                    </ul>
                    <p>
                        <?= __('Businesses of any size across industries, turn to us for the development of software solutions, tailor made for their unique challenges.','exis') ?>
                    </p>
                    <p class="text-right read-more-services"><a href="<?= get_permalink(711); ?>"><?= __('Read More', 'exis'); ?></a></p>
                    <span class="close-icon"></span>
                </div>
            </div>

        </div>




        <div class="what-section__item what-section__item--it">
            <div class="what-section__item--cont">
                <h2><?= __('Technology Consulting','exis'); ?></h2>
                <p class="mb-2"><?= __('We assist companies across industries:','exis') ?></p>
                <ul class="mb-4">
                    <li>
                        <?= __('evaluate and advance their technology infrastructure,','exis') ?>
                    </li>
                    <li>
                        <?= __('fine tune their up and running systems,','exis') ?>
                    </li>
					<li>
                        <?= __('smoothly embed new systems into their IT landscape,','exis') ?>
                    </li>
                    <li>
                        <?= __('migrate to new systems,','exis') ?>
                    </li>
                    <li>
                        <?= __('enter the digital transformation era.','exis') ?>
                    </li>
                </ul>
                <div class="arrow-section text-right">
                  <span class="arrow-icon"></span>
                </div>
            </div>
            <div class="hidden-more-info">
                <div class="hidden-more-info__inner">
                    <h2>
                        <?= __('A fresh and objective perspective on your business transformation'); ?>
                    </h2>
                    <p class="mb-2"><?= __('We bring expertise in mission critical technology related areas:','exis') ?></p>
                    <ul class="mb-4">
                        <li>
                            <?= __('Business Process Re-engineering','exis') ?>
                        </li>
                        <li>
                            <?= __('Evaluation of business support / information management systems','exis') ?>
                        </li>
                        <li>
                            <?= __('Requirements analysis & documentation','exis') ?>
                        </li>
                        <li>
                            <?= __('RFP/RFI advisory and implementation','exis') ?>
                        </li>
                    </ul>
                    <p>
                        <?= __('We know how to put all the pieces together.','exis') ?>
                    </p>
                    <p class="text-right read-more-services"><a href="<?= get_permalink(849); ?>"><?= __('Read More', 'exis'); ?></a></p>
                    <span class="close-icon"></span>
                </div>
            </div>

        </div>
        <div class="what-section__item what-section__item--eshop">
            <div class="what-section__item--cont">
                <h2><?= __('Websites | E-Shops |<br />Mobile', 'exis');?></h2>
                <p class="mb-2"><?= __('   We design, develop, deliver & maintain high-quality web applications covering the entire spectrum of:','exis') ?></p>
                <ul class="mb-4">
                    <li>
                        <?= __('websites,','exis') ?>
                    </li>
                    <li>
                        <?= __('platforms,','exis') ?>
                    </li>
                    <li>
                        <?= __('e-shops,','exis') ?>
                    </li>
                    <li>
                        <?= __('mobile applications.','exis') ?>
                    </li>
                </ul>
                <div class="arrow-section">
                  <span class="arrow-icon"></span>
                </div>
            </div>
            <div class="hidden-more-info">
                <div class="hidden-more-info__inner">
                    <h2>
                        <?= __('Bring your business online'); ?>
                    </h2>
                    <p class="mb-2" style="margin-bottom: 0!important;"><?= __('We craft custom experiences, that break through the clutter and require minimum effort to manage.</br></br>We are dedicated to delivering solutions:','exis') ?></p>
                    <ul class="mb-4">
                        <li>
                            <?= __('uniquely designed,','exis') ?>
                        </li>
                        <li>
                            <?= __('fast,','exis') ?>
                        </li>
                        <li>
                            <?= __('secure,','exis') ?>
                        </li>
                        <li>
                            <?= __('integrated with existing business software.','exis') ?>
                        </li>
                    </ul>
                       <p class="mb-4" style="margin-bottom: 0!important;"><?= __('We assist companies to stand out and attain their goals.','exis') ?></p>

                    <p class="text-right read-more-services"><a href="<?= get_permalink(483); ?>"><?= __('Read More', 'exis'); ?></a></p>
                    <span class="close-icon"></span>
                </div>
            </div>

        </div>
    </div>
    <?php
    if(!is_front_page()):
      $years_logo = get_field('exis_logo_years','option');
    ?>
    <div class="text-center years-logo-section" data-aos="zoom-in"   data-aos-once="true" data-aos-delay="100" data-aos-duration="1000">
        <img src="<?= $years_logo['url']; ?>" alt="<?= $years_logo['alt']; ?>">
    </div>
    <?php
    endif;
    ?>
</section>
