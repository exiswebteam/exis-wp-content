<div id="bottom-bar" role="contentinfo">
  <div class="copyright-section">
    <div class="wf-wrap">
      <div class="wf-container-bottom">
        <div class="wf-float-left"><a href="<?= get_home_url();?>" title="EXIS Information Technologies">EXIS</a> &copy; 2001-<?= date("Y"); ?> | All Rights Reserved. | <a href="<?= get_home_url();?>/terms-and-conditions/" title="Terms and Conditions">Terms and Conditions</a> | <a href="<?= get_home_url();?>/privacy-policy/" title="Privacy Policy">Privacy Policy</a></div>
      </div>
    </div>
  </div>
</div>
