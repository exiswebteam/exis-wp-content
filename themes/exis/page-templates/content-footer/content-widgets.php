<footer class="container site-footer" id="colophon">
  <div class="row site-info">
    <div class="col-md-3 column-footer-1">
      <?php dynamic_sidebar( 'footer-column-1' ); ?>
    </div>
    <div class="col-md-3 column-footer-2">
      <?php dynamic_sidebar( 'footer-column-2' ); ?>
    </div>
    <div class="col-md-3 column-footer-3">
      <?php dynamic_sidebar( 'footer-column-3' ); ?>
    </div>
    <div class="col-md-3 column-footer-4">
      <?php dynamic_sidebar( 'footer-column-4' ); ?>
    </div>
  </div><!-- .site-info -->
</footer><!-- #colophon -->
