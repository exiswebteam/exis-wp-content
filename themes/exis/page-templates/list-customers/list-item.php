<?php
//Defaults
$resultsIndustries = array();
$resultsServices = array();
$industriesClass = '';
$serviceClass = '';

//Get terms
$exis_industries = wp_get_post_terms(get_the_id(),'industries',array( 'fields' => 'all' ));
$exis_service = wp_get_post_terms(get_the_id(),'type',array( 'fields' => 'all' ));

//data attribute
if(!empty($exis_industries))
{
  foreach($exis_industries as $indusrty)
  {
    $resultsIndustries[] = $indusrty->slug;
  }

  $industriesClass = implode(" ",$resultsIndustries);
} else {
  $industriesClass = '';
}

//data attribute
if(!empty($exis_service))
{
  foreach($exis_service as $service)
  {
    $resultsServices[] = $service->slug;
  }

  $serviceClass = implode(",",$resultsServices);
} else {
  $serviceClass = '';
}

$link_official_site = get_field('link_official_site');
$link_exis_project = get_field('link_exis_project');
?>
<article class="customer-item <?= $industriesClass; ?>" data-service="<?= $serviceClass; ?>">
  <img class="customer-logo" src="<?= get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
  <nav class="wrap-buttons">
    <?php if(!empty($link_official_site)): ?>
    <a href="<?= $link_official_site ?>" class="exis-btn link-one" target="_blank" title="<?php the_title(); ?>"><?= __('OFFICIAL SITE','exis'); ?> <i class="fa fa-external-link" aria-hidden="true"></i></a>
    <?php endif; ?>

    <?php if(!empty($link_exis_project)): ?>
    <a href="<?= $link_exis_project; ?>" class="exis-btn link-two" target="_self" title="<?php the_title(); ?>"><?= __('EXIS PROJECT','exis'); ?> <i class="fa fa-paper-plane" aria-hidden="true"></i></a>
    <?php endif; ?>
  </nav>
</article>
