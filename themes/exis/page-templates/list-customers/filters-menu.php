<?php
//Tags - get params
$param_industries   = (isset($_GET['industries']))?urldecode($_GET['industries']): '';
$param_service   = (isset($_GET['type']))?urldecode($_GET['type']): '';

$industries_all = (!empty($param_industries))? '': ' active';
$service_all = (!empty($param_service))? '': ' active';
?>
<div class="wrap-filters-menus" data-href="<?= get_permalink(); ?>">
  <div class="list-customers-industries filter-menu">
    <h5 class="tax-menu-title"><?= __('INDUSTRIES', 'exis'); ?></h5>
    <?php
    //Term query args
    $tax_args = array(
    'taxonomy'     => 'industries',
    'orderby'      => 'name',
    'order'        => 'ASC',
    'hide_empty'   => false
    );

    //Term Query
    $terms_industry = new WP_Term_Query($tax_args);

    if(!empty($terms_industry->terms)):
      echo '<ul>';
      echo '<li><a class="filter-item all '.$industries_all.'" href="#" data-id="all" data-param="industries">ALL</a></li>';

      foreach( $terms_industry->terms as $term ):
        $count_items = $term->count;
        if($count_items>0):
          $term_active = ($param_industries==$term->term_id)? ' active':'';
        ?>
        <li>
          <a class="filter-item checkbox-btn<?= $term_active; ?>" href="#" data-id="<?= $term->term_id; ?>" data-param="industries"><?= esc_html( $term->name ); ?> <span>(<?= $count_items; ?>)</span></a>
        </li>
        <?php
        endif;
      endforeach;
      echo '</ul>';
    endif;
    ?>

  </div>

  <div class="list-customers-services filter-menu-radio">
    <h5 class="tax-menu-title"><?= __('SERVICES', 'exis'); ?></h5>
    <?php
    //Term query args
    $type_args = array(
    'taxonomy'     => 'type',
    'orderby'      => 'name',
    'order'        => 'ASC',
    'hide_empty'   => false
    );

    //Term Query
    $terms_type = new WP_Term_Query($type_args);

    if(!empty($terms_type->terms)):
      echo '<ul>';
      echo '<li><a class="filter-item all '.$service_all.'" href="#" data-id="all" data-param="type">ALL</a></li>';

      foreach( $terms_type->terms as $term ):
        $count_items = $term->count;
        if($count_items>0):
          $term_active = ($param_service==$term->term_id)? ' active':'';
        ?>
        <li>
          <a class="filter-item radio-btn<?= $term_active; ?>" href="#" data-id="<?= $term->term_id; ?>" data-param="type"><?php echo esc_html( $term->name ); ?></a>
        </li>
        <?php
        endif;
      endforeach;
      echo '</ul>';
    endif;
    ?>

  </div>

</div>
