<?php
//Term query args
$tax_args = array(
'taxonomy'     => 'industries',
'orderby'      => 'name',
'order'        => 'ASC',
'hide_empty'   => false
);

//Term Query
$terms_industry = new WP_Term_Query($tax_args);

if(!empty($terms_industry->terms)):
  foreach( $terms_industry->terms as $term ):
    $count_items = $term->count;
    $term_slug = $term->slug;

    if($count_items>0):
      echo '<section class="wrap-taxonomy" data-term="'.$term_slug.'">';
    ?>

    <h4 class="tax-menu-title"><?= $term->name; ?></h4>

    <?php
    $args = array(
        'post_type' => 'clients',
        'post_status' => 'publish',
        'orderby'      => 'name',
        'order'        => 'ASC',
        'tax_query' => array(
          array (
            'taxonomy' => 'industries',
            'field' => 'term_id',
            'terms' => $term->term_id,
          )
        ),
    );
    $query_customers = new WP_Query( $args );

    if( $query_customers->have_posts() ) :
      echo '<div class="customer-wrap">';
      while ( $query_customers->have_posts() ) : $query_customers->the_post();
      //ITEM
      get_template_part('page-templates/list-customers/list-item');
      endwhile;
      wp_reset_query();
      echo '</div>';
    endif;
    ?>

    <?php
    echo '</section>';
    endif;
  endforeach;
endif;
?>
