<?php
/**
 *
 * Template Name: Website Inner
 */


if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper" id="page-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="site-main" id="main">

                <?php get_template_part( 'page-templates/content', 'page-header' ); ?>

                <section class="container page-content-section">
                    <div class="vc_section">
                        <?php
                        the_content();
                        ?>
                    </div>
                </section>
                <section id="system-integration" class="d-flex system-integration">
                    <div class="system-integration__left">
                        <h2>
                            <?= __('Bring your business online', 'exis'); ?>
                        </h2>
                        <p>
                            <?= __('<p>We design, develop, maintain, host, and support demanding web applications covering the entire spectrum of:
	                            <ul>
	                            	<li>Websites</li>
	                            	<li>Platforms</li>
	                            	<li>E-shops (B2C & B2B)</li>
	                            	<li>Mobile applications</li>
	                            </ul>
	                            ', 'exis'); ?>
                        </p>
                        <!--<div class="system-integration__cta">
                            <a href="<?= get_permalink(368) ?>"><?= __('Need help? <br/>Contact Us!', 'exis'); ?></a>
                        </div>-->
                     <?php if(!wp_is_mobile()):?>
                     <div class="system-intergration__arrow--left">
                       <a class="arrow-left-square svg-arrow" href="#"></a>
                    </div>
                    <?php endif;?>
                    </div>
                    <div class="system-integration__cont">
                        <div class="d-flex system-integration__right">
                            <?php
                            if( have_rows('system_integration') ):
                                while ( have_rows('system_integration') ) : the_row(); ?>

                                    <div class="system-integration__item">
                                        <h3>
                                            <?php
                                            the_sub_field('title');
                                            ?>
                                        </h3>
                                        <div class="system-integration__item-desc">
                                            <?php
                                            the_sub_field('description');
                                            ?>
                                        </div>
                                    </div>
                                <?php
                                endwhile;

                            endif;

                            ?>

                        </div>
                     <?php if(wp_is_mobile()):?>
                       <p class="scroll-text">
                           <a class="arrow-left-black arrow-black" href="#"></a>
                           swipe
                           <a class="arrow-right-black arrow-black" href="#"></a>
                        </p>
                      <?php endif; ?>
                    </div>
                     <?php if(!wp_is_mobile()):?>
                     <div class="system-intergration__arrow--right">
                       <a class="arrow-right-square svg-arrow" href="#"></a>
                    </div>
                    <?php endif; ?>
                </section>
                <section id="our-process" class="d-flex  align-items-center our-process">
                    <div class="our-process__left">
                        <h2 class="mb-0">
                            <?= __('Our<br/> Process', 'exis'); ?>
                        </h2>
                    </div>
                    <div class="our-process__right">

                        <div class="step-process step-1">
                            <div class="step-process__number">
                                <span>1</span>
                            </div>
                            <div class="step-process__text">
                                <h3><?= __('Introductory Meeting', 'exis'); ?></h3>
                                <p><?= __('Our way of getting to know you. Our mission is to discover your <strong>company goals</strong>, <strong>customer profile</strong>, and your <strong>positioning</strong> in the market.', 'exis'); ?></p>
                            </div>
                        </div>
                        <div class="step-process step-2">
                            <div class="step-process__number">
                                <span>2</span>
                            </div>
                            <div class="step-process__text">
                                <h3><?= __('Design', 'exis'); ?></h3>
                                <p><?= __('Based on our findings and your content the <strong>Design Team</strong> creates <strong>alternative prototypes</strong> for you to choose from.', 'exis'); ?></p>
                            </div>
                        </div>
                        <div class="step-process step-3">
                            <div class="step-process__number">
                                <span>3</span>
                            </div>
                            <div class="step-process__text">
                                <h3><?= __('Development &amp; Maintenance', 'exis'); ?></h3>
                                <p><?= __('Our <strong>Dev Team</strong> brings the selected prototype to life by transforming the <strong>design into code</strong>. After production release, our <strong>Support Team</strong> ensures that your online business always runs smoothly.', 'exis'); ?></p>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="container px-0 websites-logo-section">
                    <h5 class="text-center"><?= __('Trusted by', 'exis'); ?></h5>
                    <div class="websites-logo">
                        <?php
                        if( have_rows('logos_websites') ):
                            while ( have_rows('logos_websites') ) : the_row(); ?>

                                <div class="websites-logo__item">
                                    <img src="<?php the_sub_field('image'); ?>" alt="<?php the_title(); ?>">
                                </div>
                            <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </section>
                <section class="websites-technologies">
                    <div class="container">
                        <h5><?= __('Technologies', 'exis'); ?></h5>
                        <div class="technologies-container">
                            <?php if( get_field('technologies') ): ?>
                               <?php the_field('technologies'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
                <section class="container px-0 websites-projects">
                    <h2><?= __('Our Track Record', 'exis'); ?></h2>
                    <div class="websites-projects__numbers">
                        <div class="text-center websites-projects__item">
                            <span><?php echo date("Y") - 2001 ?></span>
                            <p><?= __('years of experience', 'exis'); ?></p>
                        </div>
                        <div class="text-center websites-projects__item">
                            <span><?=  get_field_object('websites', 'option')['value']; ?></span>
                            <p><?=  get_field_object('websites', 'option')['label'];?></p>
                        </div>
                        <div class="text-center websites-projects__item">
                            <span><?=  get_field_object('e-shops', 'option')['value']; ?></span>
                            <p><?= get_field_object('e-shops', 'option')['label'];  ?></p>
                        </div>
                        <div class="text-center websites-projects__item">
                            <span><?=  get_field_object('web_integrations', 'option')['value']; ?></span>
                            <p><?= get_field_object('web_integrations', 'option')['label']; ?></p>
                        </div>
                    </div>
                    <div class="text-right">
                        <a class="our-work-link no-arrow" href="<?= get_category_link( 16 ) ?>">See our work</a>
                    </div>
                </section>
                <?php get_template_part( 'page-templates/content', 'page-cta' ); ?>

            </main><!-- #main -->

        </div><!-- .row -->

    </div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
