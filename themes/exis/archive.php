<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>
<div class="wrapper" id="index-wrapper">

    <div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

        <div class="row">

            <main class="container site-main" id="blog-main">
                <div class="fixed-menu">
                    <h5><?= __('Select Category', 'exis'); ?></h5>
                    <ul>
                        <li>
                            <a href="<?= get_permalink( get_option( 'page_for_posts' ) ); ?>">
                                <?= __('All', 'exis'); ?>
                            </a>
                        </li>
                        <?php
                        $terms = get_terms([
                            'taxonomy' => 'category',
                            'hide_empty' => false,
                            'exclude' => array( 1 )
                        ]);
                        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
                        $slug = basename($url);
                        foreach ( $terms as $term ) {?>
                            <li <?php echo $slug == $term->slug ? 'class="active-item"' : '' ?>>
                                <a href="<?= get_category_link($term->term_id) ?>">
                                    <?= $term->name ?>
                                </a>
                            </li>
                        <?php }
                        ?>
                    </ul>
                </div>

                <h1 class="blog-page-title">
                    <?php single_cat_title(); ?>
                </h1>
                <div class="articles-container">
                    <?php if ( have_posts() ) : ?>

                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php

                            get_template_part( 'loop-templates/content', get_post_format() );
                            ?>

                        <?php endwhile; ?>

                    <?php else : ?>

                        <?php get_template_part( 'loop-templates/content', 'none' ); ?>

                    <?php endif; ?>
                    <?php understrap_pagination(); ?>


                </div>
            </main><!-- #main -->

        </div><!-- .row -->


    </div><!-- #content -->

</div><!-- #index-wrapper -->
<?php get_template_part( 'page-templates/content', 'page-cta' ); ?>
<?php get_footer(); ?>
