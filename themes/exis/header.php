<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!--
<
███████╗██╗  ██╗██╗███████╗
██╔════╝╚██╗██╔╝██║██╔════╝
█████╗   ╚███╔╝ ██║███████╗
██╔══╝   ██╔██╗ ██║╚════██║
███████╗██╔╝ ██╗██║███████║
╚══════╝╚═╝  ╚═╝╚═╝╚══════╝  ® />
-->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="token" content="<?php echo wp_create_nonce('token'); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<?php
	if(is_front_page()){
		echo '<script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="d7bd6164-2b35-45a3-80cd-2ede3d125b38" type="text/javascript" async></script>';
	}
	?>
</head>

<body <?php body_class(); ?>>

<?php
if(is_front_page()): ?>
<div id="overlay">
	<div id="loader"></div>
</div>
<?php
endif;
?>

<?php //get_template_part('page-templates/scroll-top'); ?>
<?php get_template_part('page-templates/mobile-nav'); ?>

<?php do_action( 'wp_body_open' ); ?>

<div class="site animated fadeIn" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

		<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>

		<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-primary">

			<div class="container align-items-center">
				<?php
				the_custom_logo();
				?>
				<a class="hamburger" href="#">
					<span class="line line-1"></span>
					<span class="line line-2"></span>
					<span class="line line-3"></span>
					<span style="display: none;">Hambuerger Menu</span>
				</a>
			</div><!-- .container -->

		</nav><!-- .site-navigation -->

	</div><!-- #wrapper-navbar end -->
